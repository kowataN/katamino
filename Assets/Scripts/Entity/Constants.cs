using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Entity {
    /// <summary>
    /// プレイヤー種別
    /// </summary>
    public enum PlayerType {
        /// <summary>
        /// ホスト
        /// </summary>
        Host, 
        /// <summary>
        /// ゲスト
        /// </summary>
        Guest
    }

    /// <summary>
    /// バトル結果
    /// </summary>
    public enum BattleResult {
        /// <summary>
        /// 引き分け
        /// </summary>
        Draw,
        /// <summary>
        /// 勝利
        /// </summary>
        Win,
        /// <summary>
        /// 敗北
        /// </summary>
        Lose,
    }

    /// <summary>
    /// 画面の種類
    /// </summary>
    public enum ViewType {
        /// <summary>
        /// タイトル
        /// </summary>
        Title,
        /// <summary>
        /// 設定
        /// </summary>
        Setting,
        /// <summary>
        /// マッチング
        /// </summary>
        Matching,
        /// <summary>
        /// バトル
        /// </summary>
        Battle,
        /// <summary>
        /// ランク結果
        /// </summary>
        RankResult,
    }

    /// <summary>
    /// バトルモード
    /// </summary>
    public enum BattleMode {
        /// <summary>
        /// 一人
        /// </summary>
        Solo, 
        /// <summary>
        /// マルチ
        /// </summary>
        Multi,
    }

    /// <summary>
    /// マルチモード
    /// </summary>
    public enum MultiMode {
        /// <summary>
        /// 通常
        /// </summary>
        Normal, 
        /// <summary>
        /// ランク対戦
        /// </summary>
        Rank
    }

    /// <summary>
    /// レート変動テーブル
    /// </summary>
    public class FloatingRate {
        /// <summary>
        /// 勝利時の変動値
        /// </summary>
        public int WinValue { get; set; } = 0;

        /// <summary>
        /// 敗北時の変動値
        /// </summary>
        public int LoseValue { get; set; } = 0;
    }
}
