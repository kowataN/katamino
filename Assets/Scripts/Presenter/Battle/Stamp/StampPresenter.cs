using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class StampPresenter : MonoBehaviour
{
    [SerializeField] private StampView _view = default;
    [SerializeField] private StampPartsPresenter[] _partsPresenters = default;
    [SerializeField] private GameObject _partsObj = default;

    [SerializeField] private bool _isShow = false;

    private void Awake() {
        for (int i = 0; i < _partsPresenters.Length; i++) {
            _partsPresenters[i].Init(i);
        }
    }

    public void Init() {
        HideStampParts();
    }

    public void HideView() => _view.gameObject.SetActive(false);

    public void SetObservable() {
        // 押下処理設定
        _view.Button.OnClickAsObservable()
            .Subscribe(_ => {
                if (GameManager.Inst.BattleMode == Entity.BattleMode.Solo) {
                    return;
                }

                _isShow = !_isShow;

                _partsObj.SetActive(_isShow);
                if (_isShow) {
                    // スタンプリスト表示
                    ShowStampParts();
                } else {
                    // スタンプリスト閉じる
                    HideStampParts();
                }
            })
            .AddTo(_view.gameObject);

        for (int i=0; i<_partsPresenters.Length; i++) {
            int index = i;
            _partsPresenters[i].Button.OnClickAsObservable()
                .Subscribe(_ => {
                    if (GameManager.Inst.BattleMode == Entity.BattleMode.Solo) {
                        return;
                    }

                    Debug.Log($"スタンプ押下[{index}]");
                    PhotonManager.Inst.SendStamp(index);
                })
                .AddTo(_view.gameObject);
        }
    }

    private void ShowStampParts() {
        Debug.Log("ShowStampParts");
        foreach (var parts in _partsPresenters) {
            parts.gameObject.SetActive(true);
            parts.transform.gameObject.SetActive(true);
        }
    }

    private void HideStampParts() {
        Debug.Log("HideStampParts");
        foreach (var parts in _partsPresenters) {
            parts.gameObject.SetActive(false);
        }
    }
}
