using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniRx;

public class StampPartsPresenter : MonoBehaviour
{
    [SerializeField] private StampPartsView _view;

    public Button Button => _view.Button;

    public int StampIdx { get; set; }

    public void Init(int stampIdx) {
        StampIdx = stampIdx;
        _view.Init(stampIdx);
    }
}
