using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SearchDialogPresenter : MonoBehaviour
{
    [SerializeField] private SearchDialogView _view = default;

    public void SetActive(bool value) => _view.gameObject.SetActive(value);

    public void SetMessageActive(bool value) {
        _view.OppnentRate.gameObject.SetActive(false);
        _view.OppnentNameMsg.gameObject.SetActive(false);
        _view.PrepareMsg.gameObject.SetActive(false);
    }

    public void SetCancelButtonInteractable(bool value) => _view.Cancel.interactable = value;

    public void SetOppnentRate(bool value, string msg) {
        _view.OppnentRate.gameObject.SetActive(value);
        _view.OppnentRate.text = msg;
    }

    public void SetPrepare() {
        _view.MatchMsg.text = "対戦相手を探しています。";
        _view.SystemMsg.text = "しばらくお待ち下さい。";
        _view.OppnentNameMsg.text = "";
        _view.OppnentNameMsg.gameObject.SetActive(false);
        _view.PrepareMsg.gameObject.SetActive(false);
    }

    public void SetFoundPlayer(string oppnentName) {
        _view.MatchMsg.text = "対戦相手が見つかりました。";
        _view.SystemMsg.text = PhotonManager.Inst.IsMasterClient
                                ? "あなたはホストです。" : "あなたはゲストです。";
        _view.OppnentNameMsg.text = string.Format($"{oppnentName}さんとマッチングしました。");
        _view.OppnentNameMsg.gameObject.SetActive(true);
        _view.PrepareMsg.gameObject.SetActive(true);
    }
}
