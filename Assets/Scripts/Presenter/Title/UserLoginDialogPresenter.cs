using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserLoginDialogPresenter : MonoBehaviour
{
    [SerializeField] UserLoginDialogView _view = default;

    public void SetActive(bool value) => _view.gameObject.SetActive(value);
    public void SetLoginButtonInteractable(bool value)  => _view.BtnLogin.interactable = value;
    public void SetUserName(string name) => _view.UserName.text = name;
}
