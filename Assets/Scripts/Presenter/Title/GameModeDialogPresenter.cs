using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameModeDialogPresenter : MonoBehaviour
{
    [SerializeField] GameModeDialogView _view = default;

    public void SetActive(bool value) => _view.gameObject.SetActive(value);
}
