using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UserCreateDialogPresenter : MonoBehaviour
{
    [SerializeField] private UserCreateDialogView _view = default;

    public InputField InputName => _view.UserName;

    public void SetActive(bool value) => _view.gameObject.SetActive(value);

    public void InitStart() => _view.InitStart();

    public void InitCreate() => _view.InitCreate();

    public void SetPrepareCompleted() => _view.SetPrepareCompleted();

    public void SetUserRegistWait() => _view.SetUserRegistWait();

    public void SetSystemMsgActive(bool value) => _view.SettingMsg.gameObject.SetActive(value);

    public void SetSystemMsg(string msg) => _view.SettingMsg.text = msg;
}
