using UnityEngine;
using Photon.Pun;
using System.Collections.Generic;

public class PhotonManager : SingletonMonoBhv<PhotonManager> {
    [SerializeField] private PhotonView _photonView = default;
    [SerializeField] private BattleView _battleView = default;

    public PhotonView PhotonView { get { return _photonView; } }

    public bool IsConnected {
        get {
            if (GameManager.Inst.BattleMode == Entity.BattleMode.Solo) {
                return true;
            }
            return PhotonNetwork.IsConnected;
        }
    }

    public bool IsMasterClient {
        get {
            if (GameManager.Inst.BattleMode == Entity.BattleMode.Solo) {
                return true;
            }

            if (!IsConnected) {
                return false;
            }

            return PhotonNetwork.IsMasterClient;
        }
    }

    public Photon.Realtime.Player[] UserList {
        get { return PhotonNetwork.PlayerList; }
    }

    public void SendSetSceneView(Entity.ViewType type) {
        if (GameManager.Inst.BattleMode == Entity.BattleMode.Solo) {
            PunSetSceneView(type);
            return;
        }
        _photonView.RPC("PunSetSceneView", RpcTarget.All, type);
    }

    [PunRPC]
    private void PunSetSceneView(Entity.ViewType type) {
        GameManager.Inst.SetViewPosition(type);
    }

    public void SendMyPlayerInfo() {
        //Debug.Log($"SendMyPlayerInfo Name:{PhotonNetwork.LocalPlayer.NickName}");
        _photonView.RPC("PunSetOppnentInfo", RpcTarget.Others, GameManager.Inst.MyUserModel.CrtRate);
    }

    [PunRPC]
    private void PunSetOppnentInfo(int rate) {
        //Debug.Log($"PunSetOppnentInfo  rate:{rate}");
        GameManager.Inst.OpponentUserModels.CrtRate = rate;
    }

    public void SendSetMyPlayerIndex(int index) {
        //Debug.Log("SendSetMyPlayerIndex");
        if (!PhotonNetwork.IsMasterClient) {
            return;
        }
        _photonView.RPC("PunSetMyPlayerIndex", RpcTarget.Others, index);
    }

    [PunRPC]
    private void PunSetMyPlayerIndex(int index) {
        //Debug.Log("PunSetMyPlayerIndex : " + index.ToString());
        GameManager.Inst.SetMyPlayerIndex(index);
    }

    public void SendTransitBattle() {
        _photonView.RPC("PunTransitBattle", RpcTarget.All);
    }

    [PunRPC]
    private void PunTransitBattle() {
        //Debug.Log($"PunTransitBattle");
        GameManager.Inst.SetViewPosition(Entity.ViewType.Battle);
    }

    public void SendUpdateTurn(int turn) {
        if (GameManager.Inst.BattleMode == Entity.BattleMode.Solo) {
            //Debug.Log("SendUpdateTurn solo");
            PunUpdateTurn(turn);
            return;
        }

        //Debug.Log("SendUpdateTurn");
        _photonView.RPC("PunUpdateTurn", RpcTarget.AllViaServer, turn);
    }

    [PunRPC]
    private void PunUpdateTurn(int turn) {
        //Debug.Log($"Turn:{turn.ToString()}");
        GameManager.Inst.Turn = turn;
        _battleView.UpdateTurnMarker();
        _battleView.PlayTurnPlayerAnimation();
        if (GameManager.Inst.BattleMode == Entity.BattleMode.Multi) {
            _battleView.SetOwner();
        }
    }

    public void SendDropMino(int minoIndex, int[] squareIndex) {
        Debug.Log($"SendDropMino mino[{minoIndex}]");
        if (GameManager.Inst.BattleMode == Entity.BattleMode.Solo) {
            _battleView.SetGridViewOfMino(minoIndex, squareIndex);
            return;
        }
        _photonView.RPC("PunDropMino", RpcTarget.All, minoIndex, squareIndex);
    }

    [PunRPC]
    private void PunDropMino(int minoIndex, int[] squareIndex) {
        Debug.Log($"PunDropMino mino[{minoIndex}]");
        _battleView.SetGridViewOfMino(minoIndex, squareIndex);
    }

    public void SendBattleResult(int winPlayerIdx) {
        if (GameManager.Inst.BattleMode == Entity.BattleMode.Solo) {
            _battleView.PlayResultAnimation(winPlayerIdx);
            return;
        }

        _photonView.RPC("PunBattleResult", RpcTarget.All, winPlayerIdx);
    }

    [PunRPC]
    private void PunBattleResult(int winPlayerIdx) {
        _battleView.PlayResultAnimation(winPlayerIdx);
    }

    public void SendEndRankBattle(int winPlayerIdx) {
        _photonView.RPC("PunBattleResult", RpcTarget.All, winPlayerIdx);
    }

    public void SendStamp(int stampIdx) {
        _photonView.RPC("PunStamp", RpcTarget.Others, stampIdx);
    }

    [PunRPC]
    private void PunStamp(int stampIdx) {
        Debug.Log($"StampIdx:{stampIdx}");
        _battleView.ReceiveStampView.PlayAnime(stampIdx);
    }
}
