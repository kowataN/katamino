﻿using UnityEngine;

public class SaveDataManager : SingletonMonoBhv<SaveDataManager> {
    public void Save() => PlayerPrefs.Save();

    #region UserId
    public bool HasCustomId() {
        return PlayerPrefs.HasKey("CUSTOM_ID");
    }

    public string GetCutomId() {
        return PlayerPrefs.GetString("CUSTOM_ID");
    }

    public void SetCustomId(string userId) => PlayerPrefs.SetString("CUSTOM_ID", userId);

    public void DeleteCustomId() {
        PlayerPrefs.DeleteKey("CUSTOM_ID");
        PlayerPrefs.Save();
    }
    #endregion

    #region UserName
    public bool HasUserName() {
        return PlayerPrefs.HasKey("USER_NAME");
    }

    public string GetUserName() {
        return PlayerPrefs.GetString("USER_NAME");
    }

    public void SetUserName(string name) {
        PlayerPrefs.SetString("USER_NAME", name);
    }

    public void DeleteUserName() => PlayerPrefs.DeleteKey("USER_NAME");
    #endregion

    #region Sound
    /// <summary>
    /// 音量データを保存します。
    /// </summary>
    public void SaveAudio() {
        PlayerPrefs.SetFloat("VOL_SE", AudioManager.Inst.GetVolumeSE());
        PlayerPrefs.SetFloat("VOL_BGM", AudioManager.Inst.GetVolumeBGM());

        Save();
    }

    /// <summary>
    /// 音量データを読み込みます。
    /// </summary>
    public void LoadAudio() {
        float seVol = 0.5f;
        if (PlayerPrefs.HasKey("VOL_SE")) {
            seVol = PlayerPrefs.GetFloat("VOL_SE");
        }

        float bgmVol = 0.1f;
        if (PlayerPrefs.HasKey("VOL_BGM")) {
            bgmVol = PlayerPrefs.GetFloat("VOL_BGM");
        }

        AudioManager.Inst.SetVolumeSE(seVol);
        AudioManager.Inst.SetVolumeBGM(bgmVol);
    }
    #endregion
}
