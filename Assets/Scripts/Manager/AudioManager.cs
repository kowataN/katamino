using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : SingletonMonoBhv<AudioManager>
{
    [SerializeField] AudioClip _dropMino = default;

    [SerializeField] AudioClip _Tap = default;
    [SerializeField] AudioClip _Cancel = default;

    [SerializeField] private AudioSource _asSE = default;
    [SerializeField] private AudioSource _asBGM = default;

    public void PlayDropMino() => _asSE.PlayOneShot(_dropMino);
    public void PlayTap() => _asSE.PlayOneShot(_Tap);
    public void PlayCancel() => _asSE.PlayOneShot(_Cancel);

    public void SetVolumeBGM(float vol) => _asBGM.volume = vol;

    public void SetVolumeSE(float vol) => _asSE.volume = vol;

    public float GetVolumeBGM() => _asBGM.volume;
    public float GetVolumeSE() => _asSE.volume;
}
