﻿using Entity;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : SingletonMonoBhv<GameManager> {
    /// <summary>
    /// 入力制御
    /// </summary>
    public LockManager LockManager => _lockManager;
    private LockManager _lockManager = new LockManager();

    [SerializeField] private GameObject _viewParent = default;
    //[SerializeField] private TitleView _titleView = default;
    [SerializeField] private SettingView _settingView = default;
    [SerializeField] private MatchingView _matchingView = default;
    [SerializeField] private BattleView _battleView = default;
    [SerializeField] private RankResultView _rankResultView = default;
    [SerializeField] private LoadingView _loadingView = default;

    [SerializeField] private int _initialRate = 1000;
    public int InitialRate { get { return _initialRate; } }

    [SerializeField] private Entity.ViewType _crtViewType = Entity.ViewType.Title;
    [SerializeField] private Entity.BattleMode _battleMode = Entity.BattleMode.Solo;
    [SerializeField] private Entity.MultiMode _multiMode = Entity.MultiMode.Normal;

    public LoadingView LoadingView => _loadingView;
    
    // ターン
    // 0:先攻プレイヤー、1:後攻プレイヤー
    public int Turn { get; set; } = 0;

    public Entity.BattleMode BattleMode {
        get { return _battleMode; }
        set { _battleMode = value; }
    }

    public Entity.MultiMode MultiMode {
        get { return _multiMode; }
        set { _multiMode = value; }
    }

    private Dictionary<Entity.ViewType, float> _viewPosition = new Dictionary<Entity.ViewType, float>();
    private Dictionary<Entity.ViewType, Action> _viewAction = new Dictionary<Entity.ViewType, Action>();

    /// <summary>
    /// 自身のプレイヤーインデックス
    /// </summary>
    public int MyPlayerIndex { get { return _myPlayerIndex; } }
    private int _myPlayerIndex = 0;

    public int WinPlayerIdx {
        get { return _winPlayerIdx; }
        set { _winPlayerIdx = value; }
    }
    private int _winPlayerIdx = -1;

    public bool IsMyWin => _myPlayerIndex == _winPlayerIdx;

    /// <summary>
    /// 自身のユーザー情報
    /// </summary>
    public UserModel MyUserModel => _myUserModels;
    private UserModel _myUserModels = new UserModel();
    /// <summary>
    /// 対戦相手の情報
    /// </summary>
    public UserModel OpponentUserModels => _opponentUserModels;
    private UserModel _opponentUserModels = new UserModel();

    /// <summary>
    /// レート変動値
    /// </summary>
    public FloatingRate FloatingRate => _floatingRate;
    private FloatingRate _floatingRate = new FloatingRate();

    public string PhotonUserId { get; set; }

    public bool IsLock {
        get { return _lockManager.IsLock(); }
    }

    public bool IsMyTurn {
        get {
            if (_battleMode == Entity.BattleMode.Solo) {
                return true;
            }
            return MyPlayerIndex == GameManager.Inst.Turn;
        }
    }

    private void Awake() {
        Application.targetFrameRate = 60;

        // 各画面の表示位置
        _viewPosition.Add(Entity.ViewType.Title, 0.0f);
        _viewPosition.Add(Entity.ViewType.Setting, -1500.0f);
        _viewPosition.Add(Entity.ViewType.Matching, -3000.0f);
        _viewPosition.Add(Entity.ViewType.Battle, -4500.0f);
        _viewPosition.Add(Entity.ViewType.RankResult, -6000.0f);

        // タイトル画面遷移処理
        _viewAction.Add(Entity.ViewType.Title, () => {
            _lockManager.Lock();

            Fader.Inst.BlackIn(1.2f, () => {
                Fader.Inst.SetActiveFade(false);
                GameManager.Inst.LockManager.UnLock();
            });
        });

        // セッティング画面遷移処理
        _viewAction.Add(Entity.ViewType.Setting, () => {
            GameManager.Inst.LockManager.Lock();

            _settingView.Init();

            GameManager.Inst.LockManager.UnLock();
        });

        // マッチング画面遷移処理
        _viewAction.Add(Entity.ViewType.Matching, () => {
            GameManager.Inst.LockManager.Lock();

            _matchingView.Init();

            GameManager.Inst.LockManager.UnLock();
        });

        // バトル画面遷移処理
        _viewAction.Add(Entity.ViewType.Battle, () => {
            GameManager.Inst.LockManager.Lock();
            _battleView.Init();
            Fader.Inst.BlackIn(1.2f, () => {
                Fader.Inst.SetActiveFade(false);
                GameManager.Inst.LockManager.UnLock();
            });
        });

        // ランク結果画面遷移処理
        _viewAction.Add(Entity.ViewType.RankResult, () => {
            GameManager.Inst.LockManager.Lock();
            _rankResultView.Init();
            Fader.Inst.BlackIn(1.2f, () => {
                Fader.Inst.SetActiveFade(false);
                GameManager.Inst.LockManager.UnLock();
            });
        });

        _battleView.SetMatchingManager(_matchingView.MatchingManager);

        _loadingView.Hide();

        SetViewPosition(_crtViewType);
    }

    public void SetViewPosition(Entity.ViewType viewType) {
        _crtViewType = viewType;

        // 画面の位置調整
        float posX = _viewPosition[_crtViewType];
        _viewParent.transform.localPosition = new Vector2(posX, 0.0f);

        // 画面の初期化実行
        _viewAction[_crtViewType]?.Invoke();
    }

    public void SetMyPlayerIndex(int index) {
        _myPlayerIndex = index;
    }

    public void SetCurrentRateInfo(int rate, int ranking) {
        _myUserModels.CrtRate = rate;
        _myUserModels.CrtRanking = ranking;
    }


    public void SetMyUserInfo(int rate) {
        _myUserModels.CustomId = SaveDataManager.Inst.GetCutomId();
        _myUserModels.UserName = SaveDataManager.Inst.GetUserName();
        _myUserModels.CrtRate = rate;
    }

    public void InitOppnentInfo() {
        _opponentUserModels.CustomId = "";
        _opponentUserModels.UserName = "";
        _opponentUserModels.CrtRate = 0;
        _opponentUserModels.BattleData.Init();
    }
}
