using PlayFab;
using PlayFab.ClientModels;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Network {
    public class RankingAPI {
        public static void GetRankingList(Action<List<PlayerLeaderboardEntry>> successCallback,
            Action errorCallback) {
            PlayFabClientAPI.GetLeaderboard(
                new GetLeaderboardRequest {
                    StatisticName = "HighScoreRate",
                    StartPosition = 0,
                    MaxResultsCount = 10
                },
                result => {
                    successCallback?.Invoke(result.Leaderboard);
                },
                error => {
                    errorCallback?.Invoke();
                }
            );
        }

        /// <summary>
        /// ユーザーのランキング情報を取得します。
        /// </summary>
        public static void GetUserRanking(Action<List<PlayerLeaderboardEntry>> successCallback,
            Action errorCallback) {
            PlayFabClientAPI.GetLeaderboardAroundPlayer(
                new GetLeaderboardAroundPlayerRequest {
                    StatisticName = "HighScoreRate",
                    MaxResultsCount = 1
                },
                result => {
                    successCallback?.Invoke(result.Leaderboard);
                },
                error => errorCallback?.Invoke()
            );
        }

        public static void DeleteRankingInfo() {

        }
    }
}
