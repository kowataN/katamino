using Newtonsoft.Json;
using PlayFab;
using PlayFab.ClientModels;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Network {
    public class UserAPI : APIBase {
        /// <summary>
        /// ユーザーを作成します。
        /// </summary>
        /// <param name="customId"></param>
        public static void Create(string customId, Action<bool> callback) {
            PlayFabClientAPI.LoginWithCustomID(
                new PlayFab.ClientModels.LoginWithCustomIDRequest {
                    CustomId = customId,
                    TitleId = TitleId,
                    CreateAccount = true
                },
                result => callback?.Invoke(true),
                error => callback?.Invoke(false)
            );
        }

        /// <summary>
        ///  ユーザー名を更新します。
        /// </summary>
        /// <param name="suctomId"></param>
        /// <param name="name"></param>
        public static void UpdateUserName(string suctomId, string name, Action<bool> callback) {
            Debug.Log($"UpdateUserName name:{name}");
            PlayFabClientAPI.UpdateUserTitleDisplayName(
                new UpdateUserTitleDisplayNameRequest {
                    DisplayName = name
                },
                result => callback?.Invoke(true),
                error => {
                    Debug.Log($"{error.Error.ToString()}");
                    Debug.LogError($"{error.GenerateErrorReport()}");

                    callback?.Invoke(false);
                }
            );
        }

        /// <summary>
        /// バトルデータを取得します。
        /// </summary>
        public static void GetBattleData(Action<bool, BattleTimesModel> callback) {
            PlayFabClientAPI.GetUserData(
                new GetUserDataRequest {
                    Keys = new List<string> { "BattleData" }
                },
                result => {
                    var battleData = new BattleTimesModel();
                    if (result.Data.Count > 0) {
                        var value = result.Data["BattleData"].Value;
                        battleData = JsonConvert.DeserializeObject<BattleTimesModel>(value);
                    }
                    callback?.Invoke(true, battleData);
                },
                error => callback?.Invoke(false, null)
            );
        }

        /// <summary>
        /// バトルデータを更新します。
        /// </summary>
        /// <param name="callback"></param>
        public static void UpdateBattleData(BattleTimesModel battleInfo, Action<bool> callback) {
            var battleJson = JsonConvert.SerializeObject(battleInfo, Formatting.Indented);
            Debug.Log($"UpdateBattleData JSON:{battleJson}");
            PlayFabClientAPI.UpdateUserData(
                new UpdateUserDataRequest {
                    Data = new Dictionary<string, string> {
                        {  "BattleData", battleJson }
                    }
                },
                result => callback?.Invoke(true),
                error => callback?.Invoke(false)
            );
        }

        /// <summary>
        /// レートを更新します。
        /// </summary>
        /// <param name="value"></param>
        public static void UpdateRate(int rate, Action<bool, int> callback) {
            PlayFabClientAPI.UpdatePlayerStatistics(
                new PlayFab.ClientModels.UpdatePlayerStatisticsRequest {
                    Statistics = new List<StatisticUpdate>() {
                    new StatisticUpdate {
                        StatisticName = "HighScoreRate",
                        Value = rate
                    }
                    }
                },
                result => callback?.Invoke(true, rate),
                error => callback?.Invoke(false, rate)
            );
        }

        /// <summary>
        /// ユーザー情報を削除します。
        /// </summary>
        public static void DeleteUserInfo() {
        }
    }
}
