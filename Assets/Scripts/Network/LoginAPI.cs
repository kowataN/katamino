using Newtonsoft.Json;
using PlayFab;
using PlayFab.ClientModels;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Network {
    public class LoginAPI : APIBase {
        /// <summary>
        /// ログイン処理を行います。
        /// </summary>
        /// <param name="text"></param>
        public static void Login(string customId, Action<bool> callback) {
            PlayFabClientAPI.LoginWithCustomID(
                new PlayFab.ClientModels.LoginWithCustomIDRequest {
                    CustomId = customId,
                    TitleId = TitleId,
                    CreateAccount = false
                },
                result => callback?.Invoke(true),
                error => callback?.Invoke(false)
            );
        }
    }
}