﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class LogPanel : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {
    [SerializeField] private Button _LogClear = null;


    // Use this for initialization
    void Start() {
        _LogClear.onClick.AddListener(() =>{ LogView.Clear(); });
    }

    public void OnBeginDrag(PointerEventData eventData) {
    }

    public void OnDrag(PointerEventData eventData) {
        this.transform.position = eventData.position;
    }

    public void OnEndDrag(PointerEventData eventData) {
    }
}
