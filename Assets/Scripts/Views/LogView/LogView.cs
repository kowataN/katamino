﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class LogView : MonoBehaviour {
    private ScrollRect _ScrollRect;
    private static Text _TextLog;

    private void Awake() {
        _ScrollRect = this.gameObject.GetComponent<ScrollRect>();
        if (_ScrollRect == null) {
            Debug.LogError("_ScrollRect is null");
        }
        _TextLog = _ScrollRect.content.GetComponentInChildren<Text>();
        if (_TextLog == null) {
            Debug.LogError("_TextLog is null");
        }
    }

    private void Start() {
        Application.logMessageReceived += OnLogMessageReceived;
    }

    private void OnDestroy() {
        Application.logMessageReceived -= OnLogMessageReceived;
    }
	
    private void OnLogMessageReceived(string logText, string stackTrace, LogType logType) {
        if (string.IsNullOrEmpty(logText)) {
            return;
        }

        if (_TextLog.text.Length >= 5000) {
            Clear();
        }

        _TextLog.text += CreateTextFormat(logText, LogType.Log) + "\n";

        _ScrollRect.verticalNormalizedPosition = 0;
    }

    public static void Clear() {
        _TextLog.text = "";
    }

    /*
    public static void Log(string logText) {
        if (_TextLog == null) { return; }

        if (_TextLog.text.Length >= 5000) {
            Clear();
        }
        string logs = CreateTextFormat(logText, LogType.Log) + "\n";
        Debug.Log(logs);
    }*/

    public static void Error(string logText) {
        if (_TextLog == null) { Debug.Log("_TextLog is null"); return; }
        if (_TextLog.text.Length >= 5000) {
            Clear();
        }
        string logs = CreateTextFormat(logText, LogType.Error) + "\n";
        Debug.LogError(logs);
    }

    private static string CreateTextFormat(string log, LogType logType) {
        return log;
        // 一旦ライムスタンプを消す
        //string ret = DateTime.Now.ToString("hh:mm:ss ") + log;
        //switch (logType) {
        //    case LogType.Warning:
        //        // 黄色くしてみる
        //        break;
        //    case LogType.Error:
        //        break;
        //}
        //return ret;
    }
}
