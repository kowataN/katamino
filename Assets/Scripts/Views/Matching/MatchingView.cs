﻿using State;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MatchingView : MonoBehaviour {
    [SerializeField] private MatchingManager _matchingManager = default;
    [SerializeField] private Button _btnBack = default;
    [Header("ランダム対戦")]
    [SerializeField] private Button _btnRandom = default;
    [Header("フレンド対戦")]
    [SerializeField] private InputField _inputFriend = default;
    [SerializeField] private Button _btnFriend = default;
    [Header("ランク対戦")]
    [SerializeField] private Button _btnRank = default;
    [SerializeField] private Text _txtRanking = default;
    [SerializeField] private Text _txtBattleTimes = default;
    [SerializeField] private Text _txtWinTimes = default;
    [SerializeField] private Text _txtLoseTimes = default;
    [Header("サーチパネル")]
    [SerializeField] private SearchDialogPresenter _searchDialogPresenter = default;

    [SerializeField] private DebugRateView _debugRate = default;
    [SerializeField] private DebugBattleDataView _debugBattleData = default;

    public MatchingManager MatchingManager => _matchingManager;

    private enum MatchMsgState {
        Search, FoundPlayer
    }

    private void Awake() {
        _matchingManager.CallBackFoundPlayer.AddListener(FoundPlayer);
        _matchingManager.CallBackLeftPlayer.AddListener(LeftPlayer);
        _matchingManager.CallBackJoinRoom.AddListener(JoinRoom);

        _debugRate.OnClickAction = () => { SetRankText(); };
        _debugBattleData.CallBackWinTimes = (int value) => {
            if (GameManager.Inst.MyUserModel.BattleData.WinTimes == 0 && value == -1) {
                return;
            }
            GameManager.Inst.MyUserModel.BattleData.WinTimes += value;
            SetBattleData();
        };
        _debugBattleData.CallBackLoseTimes = (int value) => {
            if (GameManager.Inst.MyUserModel.BattleData.LoseTimes == 0 && value == -1) {
                return;
            }
            GameManager.Inst.MyUserModel.BattleData.LoseTimes += value;
            SetBattleData();
        };
        _debugBattleData.CallBackUpdate = () => {
            GameManager.Inst.LoadingView.Show();
            Network.UserAPI.UpdateBattleData(GameManager.Inst.MyUserModel.BattleData, (bool value) => {
                GameManager.Inst.LoadingView.Hide();
                Debug.Log($"UpdateBattleData  result:{value}");
            });
        };
    }

    private void OnDestroy() {
        _matchingManager.CallBackFoundPlayer.RemoveListener(FoundPlayer);
        _matchingManager.CallBackLeftPlayer.RemoveListener(LeftPlayer);
        _matchingManager.CallBackJoinRoom.RemoveListener(JoinRoom);
    }

    public void Init() {
        GameManager.Inst.BattleMode = Entity.BattleMode.Multi;
        GameManager.Inst.MultiMode = Entity.MultiMode.Normal;

        _searchDialogPresenter.SetActive(false);
        SetMatchMsg(MatchMsgState.Search);
        _searchDialogPresenter.SetCancelButtonInteractable(false);
        _btnRandom.interactable = true;
        _btnRank.interactable = true;
        _btnBack.interactable = true;

        _searchDialogPresenter.SetMessageActive(false);

        _inputFriend.text = "";
        _btnFriend.interactable = false;

        SetRankText();
        SetBattleData();

        _debugRate?.SetRate();

        if (PhotonManager.Inst.IsConnected) {
            // 既に接続している場合は一旦退室する
            OnClickCancel();
        }
    }

    private void JoinRoom() {
        _searchDialogPresenter.SetCancelButtonInteractable(true);
    }

    private void FoundPlayer() {
        //Debug.Log("FoundPlayer");
        SetMatchMsg(MatchMsgState.FoundPlayer);

        _searchDialogPresenter.SetCancelButtonInteractable(false);

        StartCoroutine(TransitBattle());
    }

    IEnumerator TransitBattle() {
        GameManager.Inst.LoadingView.Show();
        PhotonManager.Inst.SendMyPlayerInfo();
        while (GameManager.Inst.OpponentUserModels.CrtRate == 0) {
            yield return new WaitForSeconds(0.1f);
        }

        _searchDialogPresenter.SetOppnentRate(true, "レート: " + GameManager.Inst.OpponentUserModels.CrtRate.ToString());

        yield return new WaitForSeconds(3f);

        if (PhotonManager.Inst.IsMasterClient) {
            OnClickNext();
        }
        GameManager.Inst.LoadingView.Hide();

        yield break;
    }

    private void LeftPlayer() => SetMatchMsg(MatchMsgState.Search);

    /// <summary>
    /// すべてのボタンの有効状態を設定します。
    /// </summary>
    /// <param name="value"></param>
    private void SetButtonAllEnabled(bool value) {
        _btnFriend.interactable = value;
        _btnRandom.interactable = value;
        _btnRank.interactable = value;
        _btnBack.interactable = value;
    }

    /// <summary>
    /// ランダム対戦押下処理を行います。
    /// </summary>
    public void OnClickRandom() {
        GameManager.Inst.MultiMode = Entity.MultiMode.Normal;
        GameManager.Inst.InitOppnentInfo();
        _matchingManager.IsMatching = true;
        SetButtonAllEnabled(false);

        if (!PhotonManager.Inst.IsConnected) {
            _matchingManager.Connected(_inputFriend.text, _txtRanking.text,
                MatchingManager.MatchMode.Random);
        } else {
            _matchingManager.MatchingRandom();
        }

        _searchDialogPresenter.SetActive(true);
    }

    /// <summary>
    /// フレンド対戦押下処理を行います。
    /// </summary>
    public void OnClickFriend() {
        GameManager.Inst.MultiMode = Entity.MultiMode.Normal;
        GameManager.Inst.InitOppnentInfo();
        _matchingManager.IsMatching = true;
        SetButtonAllEnabled(false);
        SetMatchMsg(MatchMsgState.Search);

        if (!PhotonManager.Inst.IsConnected) {
            _matchingManager.Connected(_inputFriend.text, _txtRanking.text,
                MatchingManager.MatchMode.Friend);
        } else {
            _matchingManager.MatchingFriend(_inputFriend.text);
        }

        _searchDialogPresenter.SetActive(true);
    }

    /// <summary>
    /// ランク対戦押下を行います。
    /// </summary>
    public void OnClickRank() {
        GameManager.Inst.MultiMode = Entity.MultiMode.Rank;
        GameManager.Inst.InitOppnentInfo();
        _matchingManager.IsMatching = true;
        SetButtonAllEnabled(false);
        SetMatchMsg(MatchMsgState.Search);

        if (!PhotonManager.Inst.IsConnected) {
            _matchingManager.Connected(_inputFriend.text, _txtRanking.text,
                MatchingManager.MatchMode.Rank);
        } else {
            _matchingManager.MatchingRank(_txtRanking.text);
        }

        _searchDialogPresenter.SetActive(true);
    }

    /// <summary>
    /// 戻るボタン押下処理を行います。
    /// </summary>
    public void OnClickBack() {
        if (PhotonManager.Inst.IsConnected) {
            GameManager.Inst.LoadingView.Show();
            _matchingManager.CallBackDisconnected.AddListener(CallBackDisconnected);
            _matchingManager.Disconnect();
            return;
        }

        GameManager.Inst.SetViewPosition(Entity.ViewType.Title);
    }

    private void CallBackDisconnected() {
        GameManager.Inst.LoadingView.Hide();
        _matchingManager.CallBackDisconnected.RemoveListener(CallBackDisconnected);
        GameManager.Inst.SetViewPosition(Entity.ViewType.Title);
    }

    public void OnClickCancel() {
        _matchingManager.IsMatching = false;

        UpdateFriendButtonEnabled();
        _btnRandom.interactable = true;
        _btnRank.interactable = true;
        _btnBack.interactable = true;
        _searchDialogPresenter.SetCancelButtonInteractable(true);

        SetMatchMsg(MatchMsgState.Search);

        if (PhotonManager.Inst.IsConnected) {
            _matchingManager.LeaveRoom();
        }

        _searchDialogPresenter.SetActive(false);
    }

    public void OnClickNext() {
        if (PhotonManager.Inst.IsMasterClient) {
            GameManager.Inst.Turn = UnityEngine.Random.Range(0, 2);

            var playerList = PhotonManager.Inst.UserList;
            for (int i = 0; i < playerList.Length; i++) {
                if (playerList[i].UserId == GameManager.Inst.PhotonUserId) {
                    GameManager.Inst.SetMyPlayerIndex(i);
                    break;
                }
            }

            // 対戦相手のindexを送る
            PhotonManager.Inst.SendSetMyPlayerIndex(1 - GameManager.Inst.MyPlayerIndex);
        }

        PhotonManager.Inst.SendTransitBattle();
    }

    public void OnValueChangedInput() {
        UpdateFriendButtonEnabled();
    }

    public void OnEndEditInput() {
        Debug.Log("OnEndEditInput");
        UpdateFriendButtonEnabled();
    }
    private void UpdateFriendButtonEnabled() {
        _btnFriend.interactable = (_inputFriend.text.Length == 4);
    }

    private void SetMatchMsg(MatchMsgState state) {
        if (state == MatchMsgState.FoundPlayer) {
            _searchDialogPresenter.SetFoundPlayer(_matchingManager.GetOppnent().NickName);
        } else {
            _searchDialogPresenter.SetPrepare();
        }
    }

    private void SetBattleData() {
        var battleData = GameManager.Inst.MyUserModel.BattleData;
        if (battleData != null) {
            _txtBattleTimes.text = battleData.BattleTimes.ToString();
            _txtWinTimes.text = battleData.WinTimes.ToString();
            _txtLoseTimes.text = battleData.LoseTimes.ToString();
        }
    }

    private void SetRankText() {
        _txtRanking.text = RankString.GetString(GameManager.Inst.MyUserModel.CrtRate);
        if (RankColors.ColorList.ContainsKey(_txtRanking.text)) {
            _txtRanking.color = RankColors.ColorList[_txtRanking.text];
        }
    }
}
