using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.Events;

public class MatchingManager : MonoBehaviourPunCallbacks {
    private string _roomName = default;
    private RoomOptions _roomOptions = new RoomOptions();
    private bool _isMatching = false;
    public string Rank {
        get { 
            return _rank; 
        }
    }
    private string _rank = default;
    public enum MatchMode {
        Friend, Random, Rank
    };
    private MatchMode _matchMode = MatchMode.Friend;

    public UnityEvent CallBackFoundPlayer;
    public UnityEvent CallBackLeftPlayer;
    public UnityEvent CallBackJoinRoom;
    public UnityEvent CallBackDisconnected;

    public bool IsMatching {
        set { _isMatching = value; }
        get { return _isMatching; }
    }

    private void Start() {
        _roomOptions.MaxPlayers = 2;
        _roomOptions.PublishUserId = true;
    }

    /// <summary>
    /// サーバーに接続します。
    /// </summary>
    /// <param name="roomName"></param>
    /// <param name="rank"></param>
    /// <param name="mode"></param>
    public void Connected(string roomName, string rank, MatchMode mode) {
        _roomName = roomName;
        _rank = rank;
        _matchMode = mode;
        PhotonNetwork.NickName = SaveDataManager.Inst.GetUserName();

        PhotonNetwork.ConnectUsingSettings();
        if (string.IsNullOrEmpty(PhotonNetwork.NickName)) {
            PhotonNetwork.NickName = "Player_" + Random.Range(1, 999999);
            //PhotonNetwork.NickName = SaveDataManager.Instance.GetUserName();
        }
    }

    public void Disconnect() {
        if (PhotonNetwork.IsConnected) {
            PhotonNetwork.Disconnect();
        }
    }

    public override void OnDisconnected(DisconnectCause cause) {
        Debug.Log($"OnDisconnected");
        CallBackDisconnected?.Invoke();
    }

    /// <summary>
    /// フレンドマッチングを行います。
    /// </summary>
    /// <param name="roomName"></param>
    public void MatchingFriend(string roomName) {
        _roomOptions.IsVisible = false;
        _roomOptions.CustomRoomProperties = null;
        PhotonNetwork.JoinOrCreateRoom(roomName, _roomOptions, TypedLobby.Default);
    }

    /// <summary>
    /// ランダムマッチングを行います。
    /// </summary>
    public void MatchingRandom() {
        _roomOptions.IsVisible = false;
        _roomOptions.CustomRoomProperties = null;
        PhotonNetwork.JoinRandomRoom();
    }

    /// <summary>
    /// ランダムマッチ用の部屋を作成します。
    /// </summary>
    /// <param name="roomName"></param>
    private void CreateRandomRoom(string roomName) {
        _roomOptions.IsVisible = true;
        _roomOptions.CustomRoomProperties = null;
        PhotonNetwork.CreateRoom(roomName, _roomOptions, TypedLobby.Default);
    }

    private void CreateRankRoom() {
        Debug.Log("CreateRankRoom");
        _roomOptions.IsVisible = true;
        _roomOptions.CustomRoomProperties = new ExitGames.Client.Photon.Hashtable(){
            { "Rank", (string)Rank },
        };

        _roomOptions.CustomRoomPropertiesForLobby = new string[] { "Rank" };
        PhotonNetwork.CreateRoom(null, _roomOptions, TypedLobby.Default);
    }

    /// <summary>
    /// ランクマッチングを行います。
    /// </summary>
    public void MatchingRank(string rank) {
        _rank = rank;
        var hash = new ExitGames.Client.Photon.Hashtable(){
            { "Rank", (string)Rank },
        };

        _roomOptions.IsVisible = true;
        PhotonNetwork.JoinRandomRoom(hash, 2);
    }

    public void LeaveRoom() {
        if (PhotonNetwork.InRoom) {
            PhotonNetwork.LeaveRoom();
        }
    }

    /// <summary>
    /// 対戦相手を返します。
    /// </summary>
    /// <returns></returns>
    public Player GetOppnent() {
        return PhotonNetwork.PlayerListOthers[0];
    }

    public override void OnConnectedToMaster() {
        if (_isMatching) {
            switch (_matchMode) {
                // ランダムマッチ
                case MatchMode.Random:
                    MatchingRandom();
                    break;

                // ランクマッチ
                case MatchMode.Rank:
                    MatchingRank(_rank);
                    break;

                // フレンドマッチ
                default:
                    MatchingFriend(_roomName);
                    break;
            }
        }
    }

    public override void OnJoinedRoom() {
        //Debug.Log("OnJoinedRoom : " + PhotonNetwork.CurrentRoom.PlayerCount.ToString() +"人");
        var hash = PhotonNetwork.CurrentRoom.CustomProperties;
        foreach (var prop in PhotonNetwork.CurrentRoom.CustomProperties) {
            Debug.Log($"options: key[{prop.Key.ToString()}] val[{prop.Value.ToString()}]");
        }

        if (GameManager.Inst) {
            GameManager.Inst.PhotonUserId = PhotonNetwork.LocalPlayer.UserId;
        }

        CallBackJoinRoom?.Invoke();
        if (PhotonNetwork.CurrentRoom.PlayerCount == PhotonNetwork.CurrentRoom.MaxPlayers) {
            PhotonNetwork.CurrentRoom.IsOpen = false;
            //Debug.Log("CurrentRoom.IsOpen:" + PhotonNetwork.CurrentRoom.IsOpen.ToString());

            CallBackFoundPlayer?.Invoke();
        }
    }

    public override void OnLeftRoom() {
        Debug.Log("OnLeftRoom");
    }

    public override void OnPlayerEnteredRoom(Player newPlayer) {
        Debug.Log($"OnPlayerEnteredRoom player:{newPlayer.NickName}");

        if (PhotonNetwork.CurrentRoom.PlayerCount == PhotonNetwork.CurrentRoom.MaxPlayers) {
            PhotonNetwork.CurrentRoom.IsOpen = false;
            Debug.Log("CurrentRoom.IsOpen:" + PhotonNetwork.CurrentRoom.IsOpen.ToString());

            CallBackFoundPlayer?.Invoke();
        }
    }

    public override void OnPlayerLeftRoom(Player otherPlayer) {
        Debug.Log($"OnPlayerLeftRoom player:{otherPlayer.NickName}");
        PhotonNetwork.CurrentRoom.IsOpen = true;
        Debug.Log("CurrentRoom.IsOpen:" + PhotonNetwork.CurrentRoom.IsOpen.ToString());
        CallBackLeftPlayer?.Invoke();
    }

    public override void OnJoinRandomFailed(short returnCode, string message) {
        Debug.Log("OnJoinRandomFailed");
        if (_matchMode == MatchMode.Random) {
            CreateRandomRoom(null);
        } else {
            CreateRankRoom();
        }
    }

    public override void OnJoinRoomFailed(short returnCode, string message) {
        Debug.Log("OnJoinRoomFailed");
    }
}
