using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugMatchingView : MonoBehaviour
{
    [Header("基本情報")]
    [SerializeField] private InputField _inputPlayerRate = default;
    [SerializeField] private InputField _inputEnemyRate = default;
    //[SerializeField] private Button _btnAllCalc = default;
    //[SerializeField] private Button _btnAllReset = default;
    [Header("イロレーティング関連")]
    [SerializeField] private InputField _inputIroConst = default;
    [SerializeField] private Text _txtIroPlayerDiff = default;
    [SerializeField] private Text _txtIroEnemyDiff = default;
    [SerializeField] private Text _txtIroPlayerSum = default;
    [SerializeField] private Text _txtIroEnemySum = default;
    [Header("グリコレーティング関連")]
    [SerializeField] private InputField _inputGlickoConst = default;
    [SerializeField] private Text _txtGlickoPlayerDiff = default;
    [SerializeField] private Text _txtGlickoEnemyDiff = default;
    [SerializeField] private Text _txtGlickoPlayerSum = default;
    [SerializeField] private Text _txtGlickoEnemySum = default;

    private CalcRateGlicko _calcGlicko = new CalcRateGlicko();

    private void Awake() {
        Reset();
    }

    private void Reset() {
        _inputPlayerRate.text = "1500";
        _inputEnemyRate.text = "1500";

        _inputIroConst.text = "16";
        _txtIroPlayerDiff.text = "0";
        _txtIroPlayerDiff.color = Color.black;
        _txtIroEnemyDiff.text = "0";
        _txtIroEnemyDiff.color = Color.black;
    }

    public void OnClickAllCalc() {
        Debug.Log("OnClickAllCalc");
    }

    public void OnClickReset() {
        Debug.Log("OnClickReset");
        Reset();
    }

    public void OnClickIro(bool isWin) {
        CalcRateIro.FloatingValue = int.Parse(_inputIroConst.text);

        int playerRate = int.Parse(_inputPlayerRate.text);
        int enemyRate = int.Parse(_inputEnemyRate.text);

        int winVal = CalcRateIro.CalcRate(playerRate, enemyRate);
        int loseVal = CalcRateIro.CalcRate(enemyRate, playerRate);
        Debug.Log($"winVal:{winVal} loseVal:{loseVal}");

        _txtIroPlayerDiff.text = isWin ? winVal.ToString() : loseVal.ToString();
        _txtIroEnemyDiff.text = isWin ? loseVal.ToString() : winVal.ToString();
        _txtIroPlayerDiff.color = isWin ? Color.blue : Color.red;
        _txtIroEnemyDiff.color = isWin ? Color.red : Color.blue;

        if (isWin) {
            _txtIroPlayerSum.text = string.Format($"{_inputPlayerRate.text} → {playerRate + winVal}");
            _txtIroEnemySum.text = string.Format($"{_inputEnemyRate.text} → {enemyRate - loseVal}");
        } else {
            _txtIroPlayerSum.text = string.Format($"{_inputPlayerRate.text} → {playerRate - loseVal}");
            _txtIroEnemySum.text = string.Format($"{_inputEnemyRate.text} → {enemyRate + winVal}");
        }

        int newRate = 0;
        if (isWin) {
            newRate = Mathf.Clamp(playerRate + winVal, 1000, playerRate + winVal);
            _inputPlayerRate.text = newRate.ToString();
            newRate = Mathf.Clamp(enemyRate - loseVal, 1000, enemyRate - loseVal);
            _inputEnemyRate.text = newRate.ToString();
        } else {
            newRate = Mathf.Clamp(playerRate - winVal, 1000, playerRate - loseVal);
            _inputPlayerRate.text = newRate.ToString();
            newRate = Mathf.Clamp(enemyRate + loseVal, 1000, enemyRate + winVal);
            _inputEnemyRate.text = newRate.ToString();
        }
    }

    public void OnClickGlicko(bool isWin) {
        _calcGlicko.Deviation = int.Parse(_inputGlickoConst.text);
        _txtGlickoPlayerDiff.text = "";
        _txtGlickoPlayerSum.text = "";
        _txtGlickoEnemyDiff.text = "";
        _txtGlickoEnemySum.text = "";

        int playerVal, enemyVal;
        CalcGlicko(isWin, out playerVal, out enemyVal);

        Debug.Log($"playerVal:{playerVal} enemyVal:{enemyVal}");
    }

    private void CalcGlicko(bool isWinPlayer, out int winVal, out int loseVal) {
        int playerRate = int.Parse(_inputPlayerRate.text);
        int enemyRate = int.Parse(_inputEnemyRate.text);

        int rate1 = isWinPlayer ? playerRate : enemyRate;
        int rate2 = isWinPlayer ? enemyRate : playerRate;

        winVal = _calcGlicko.CalcRate(rate1, rate2);
        loseVal = _calcGlicko.CalcRate(rate1, rate2);
    }
}
