using System;
using UnityEngine;

public class DebugBattleDataView : MonoBehaviour {
    public Action<int> CallBackWinTimes;
    public Action<int> CallBackLoseTimes;
    public Action CallBackUpdate;

    public void OnClickWinTimes(int value) => CallBackWinTimes?.Invoke(value);
    public void OnClickLoseTimes(int value) => CallBackLoseTimes?.Invoke(value);
    public void OnClickUpdate() => CallBackUpdate?.Invoke();
}
