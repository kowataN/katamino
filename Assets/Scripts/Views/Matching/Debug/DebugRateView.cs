using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugRateView : MonoBehaviour
{
    [SerializeField] private Text _txtCrtRate = default;
    [SerializeField] private InputField _inputRate = default;

    public Action OnClickAction = default;

    private void OnDestroy() =>OnClickAction = null;

    public void OnClickUpdateRate(int value) {
        GameManager.Inst.LoadingView.Show();

        Network.UserAPI.UpdateRate(value, (bool result, int rateValue) => {
            GameManager.Inst.LoadingView.Hide();
            if (result) {
                GameManager.Inst.SetMyUserInfo(rateValue);
                _txtCrtRate.text = "現在:" + rateValue.ToString();

                OnClickAction?.Invoke();
            }
        });
    }

    public void OnClickInputRate() {
        int inputRate = int.Parse(_inputRate.text);
        OnClickUpdateRate(inputRate);
    }

    public void SetRate() {
        _txtCrtRate.text = "現在:" + GameManager.Inst.MyUserModel.CrtRate.ToString();
        _inputRate.text = "";
    }
}
