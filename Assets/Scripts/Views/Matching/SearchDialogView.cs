using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SearchDialogView : MonoBehaviour
{
    [SerializeField] private Text _txtMatchMsg = default;
    [SerializeField] private Text _txtSystemMsg = default;
    [SerializeField] private Button _btnCancel = default;
    [SerializeField] private Text _txtOppnentRate = default;
    [SerializeField] private Text _txtOppnentNameMsg = default;
    [SerializeField] private Text _txtPrepareMsg = default;

    public Text MatchMsg => _txtMatchMsg;
    public Text SystemMsg => _txtSystemMsg;

    public Text OppnentRate => _txtOppnentRate;
    public Text OppnentNameMsg => _txtOppnentNameMsg;
    public Text PrepareMsg => _txtPrepareMsg;

    public Button Cancel => _btnCancel;
}
