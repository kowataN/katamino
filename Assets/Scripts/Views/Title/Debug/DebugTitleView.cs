using PlayFab.ClientModels;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class DebugTitleView : MonoBehaviour {
    [Header("ユーザー関連")]
    [SerializeField] private InputField _customId = default;
    [SerializeField] private InputField _userName = default;
    [SerializeField] private Text _devieceId = default;
    [SerializeField] private Text _loginMsg = default;
    [SerializeField] private Button _btnLogin = default;
    [SerializeField] private Button _btnUpdateName = default;

    [Header("レート関連")]
    [SerializeField] private List<Button> _btnRateList = default;

    [Header("ランキング関連")]
    [SerializeField] private Text _rankingText = default;
    [SerializeField] private Button _btnUpdateRanking = default;

    private void Awake() {
        _devieceId.text = SystemInfo.deviceUniqueIdentifier;
        Debug.Log(_devieceId.text);
        _loginMsg.text = "[未ログイン]";
        _btnLogin.interactable = false;
        _btnUpdateName.interactable = false;
        _userName.enabled = false;
        _btnRateList.ForEach(btn => btn.interactable = false);
    }

    /// <summary>
    /// カスタムID入力
    /// </summary>
    public void OnValueChangedCustomId() {
        _btnLogin.interactable = (_customId.text.Length > 1);
    }

    /// <summary>
    /// 名前入力
    /// </summary>
    public void OnValueChangedUserName() {
        _btnUpdateName.interactable = (_userName.text.Length > 1);
    }

    /// <summary>
    /// ログインボタン押下
    /// </summary>
    public void OnCLickLogin() {
        _btnLogin.interactable = false;
        //PlayFabManager.Inst.Login(_customId.text);
        Network.LoginAPI.Login(_customId.text, (bool result) => {
            string strResult = result ? "成功" : "失敗";
            Debug.Log("ログイン" + strResult);
            _btnLogin.interactable = result;
            _userName.enabled = result;
            _btnUpdateRanking.interactable = result;
            _btnRateList.ForEach(btn => btn.interactable = result);
            _loginMsg.text = "[ログイン" + strResult + "]";
        });
    }

    /// <summary>
    /// 名前更新ボタン押下
    /// </summary>
    public void OnClickUpdateUserName() {
        _btnUpdateName.interactable = false;
        //PlayFabManager.Inst.UpdateUserName(_customId.text, _userName.text);
        Network.UserAPI.UpdateUserName(_customId.text, _userName.text, (bool result) => {
            Debug.Log("名前更新" + (result ? "成功" : "失敗"));
            _btnUpdateName.interactable = true;
        });
    }

    /// <summary>
    /// レート更新ボタン押下
    /// </summary>
    /// <param name="value"></param>
    public void OnClickUpdateRate(int value) {
        _btnRateList.ForEach(btn => btn.interactable = false);
        //PlayFabManager.Inst.UpdateRate(value);
        Network.UserAPI.UpdateRate(value, (bool result, int rate) => {
            Debug.Log("レート更新" + (result ? "成功" : "失敗"));
            _btnRateList.ForEach(btn => btn.interactable = true);
        });
    }

    /// <summary>
    /// ランキング更新ボタン押下
    /// </summary>
    public void OnClickUpdateRanking() {
        _rankingText.text = "";
        _btnUpdateRanking.interactable = false;
        Network.RankingAPI.GetRankingList(
            // 通信成功
            (List<PlayerLeaderboardEntry> rankingList) => {
                Debug.Log("ランキング取得成功");
                foreach (var entry in rankingList) {
                    _rankingText.text += $"{entry.Position + 1,5}位：{entry.StatValue,5}：{entry.DisplayName}\n";
                }
                _btnUpdateRanking.interactable = true;
            },
            // 通信失敗
            () => {
                Debug.Log("ランキング取得失敗");
                _rankingText.text = "ランキングデータ取得に失敗";
                _btnUpdateRanking.interactable = true;
            });
    }
}
