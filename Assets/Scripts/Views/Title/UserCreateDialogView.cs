using UnityEngine;
using UnityEngine.UI;

public class UserCreateDialogView : MonoBehaviour
{
    [SerializeField] private Text _txtSettingMsg = default;
    [SerializeField] private InputField _inputUserName = default;
    [SerializeField] private Button _btnNext = default;
    [SerializeField] private Button _btnRegist = default;

    public Text SettingMsg => _txtSettingMsg;
    public InputField UserName => _inputUserName;

    public void SetActive(bool value) {
        this.gameObject.SetActive(value);
    }

    public void InitStart() {
        _txtSettingMsg.gameObject.SetActive(false);
    }

    public void InitCreate() {
        _txtSettingMsg.gameObject.SetActive(false);
        _btnNext.gameObject.SetActive(false);
        _btnNext.interactable = false;
        _btnRegist.gameObject.SetActive(true);
        _btnRegist.interactable = true;
    }

    public void SetPrepareCompleted() {
        _txtSettingMsg.text = "準備が整いました。";
        _btnRegist.gameObject.SetActive(false);
        _btnRegist.interactable = false;
        _btnNext.gameObject.SetActive(true);
        _btnNext.interactable = true;
        _inputUserName.enabled = false;
    }

    public void SetUserRegistWait() {
        _txtSettingMsg.gameObject.SetActive(true);
        _txtSettingMsg.text = "ユーザーを登録中です。";
        _inputUserName.enabled = false;
    }
}
