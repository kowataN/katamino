using UnityEngine;
using UnityEngine.UI;

public class UserLoginDialogView : MonoBehaviour {
    [SerializeField] private Text _txtUserName = default;
    [SerializeField] private Button _btnLogin = default;

    public Button BtnLogin => _btnLogin;
    public Text UserName => _txtUserName;
}
