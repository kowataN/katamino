﻿using PlayFab.ClientModels;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TitleView : MonoBehaviour {
    [SerializeField] private GameObject _panelTapStart = default;
    [SerializeField] private UserCreateDialogPresenter _userCreateDialogPresenter = default;
    [SerializeField] private UserLoginDialogPresenter _userLoginDialogPresenter = default;
    [SerializeField] private GameModeDialogPresenter _gameModeDialogPresenter = default;

    private string _customId = "";

    private enum TitleMode {
        Start, UserCreate, UserLogin, GameMode
    };
    private TitleMode _titleMode = TitleMode.Start;


    private void Awake() {
        SaveDataManager.Inst.LoadAudio();
        Init();
    }

    public void Init() {
        _customId = "";

        SetTitleMode(TitleMode.Start);
    }

    public void OnClickTapStart() {
        //Debug.Log("OnClickTapStart");
        if (GameManager.Inst.LockManager.IsLock()) {
            return;
        }

        if (SaveDataManager.Inst.HasCustomId()) {
            //Debug.Log($"UserID:{SaveDataManager.Inst.GetCutomId()}");
            _customId = SaveDataManager.Inst.GetCutomId();
            SetTitleMode(TitleMode.UserLogin);
        } else {
            SetTitleMode(TitleMode.UserCreate);
        }
    }

    private void SetTitleMode(TitleMode mode) {
        SetPanelActive(mode);

        switch (mode) {
            case TitleMode.Start:
                _userCreateDialogPresenter.InitStart();
                break;

            case TitleMode.UserCreate:
                _userCreateDialogPresenter.InitCreate();
                break;

            case TitleMode.UserLogin:
                GameManager.Inst.MyUserModel.UserName = SaveDataManager.Inst.GetUserName();
                _userLoginDialogPresenter.SetLoginButtonInteractable(true);
                _userLoginDialogPresenter.SetUserName("ようこそ、" + GameManager.Inst.MyUserModel.UserName + "さん。");
                _userCreateDialogPresenter.SetSystemMsgActive(false);
                break;
        }
        _titleMode = mode;
    }

    private void SetPanelActive(TitleMode mode) {
        _panelTapStart.SetActive(mode == TitleMode.Start);
        _userCreateDialogPresenter.SetActive(mode == TitleMode.UserCreate);
        _userLoginDialogPresenter.SetActive(mode == TitleMode.UserLogin);
        _gameModeDialogPresenter.SetActive(mode == TitleMode.GameMode);
    }

    public void OnClickRegist() {
        _userCreateDialogPresenter.SetUserRegistWait();
        System.Guid guid = System.Guid.NewGuid();
        _customId = guid.ToString();
        // ユーザー作成
        Network.UserAPI.Create(_customId, (bool value) => {
            if (value) {
                _userCreateDialogPresenter.SetSystemMsg("ユーザー情報を初期化しています。");

                // ユーザー名変更
                Network.UserAPI.UpdateUserName(_customId, _userCreateDialogPresenter.InputName.text, (bool resultUserName) => {
                    if (resultUserName) {
                        _userCreateDialogPresenter.SetSystemMsg("ランキング情報を初期化しています。");
                        if (!SaveDataManager.Inst.HasCustomId()) {
                            SaveDataManager.Inst.SetCustomId(_customId);
                        }
                        SaveDataManager.Inst.SetUserName(_userCreateDialogPresenter.InputName.text);
                        PlayerPrefs.Save();
                        GameManager.Inst.MyUserModel.UserName = _userCreateDialogPresenter.InputName.text;

                        // レート更新
                        Network.UserAPI.UpdateRate(GameManager.Inst.InitialRate, (bool resultRate, int rete) => {
                            if (resultRate) {
                                _userCreateDialogPresenter.SetPrepareCompleted();
                            } else {
                                Debug.LogError("CallbackAddRate Error");
                            }
                        });
                    } else {
                        Debug.LogError("CallbackUpdateName Error");
                    }
                });
                GameManager.Inst.MyUserModel.UserName = _userCreateDialogPresenter.InputName.text;
            } else {
                // もし失敗した場合は成功するまで登録を繰り返す。
                // TODO: 後で回数制限をつける
                OnClickRegist();
            }
        });
    }

    public void OnClickLogin() {
        GameManager.Inst.LoadingView.Show();
        _userLoginDialogPresenter.SetLoginButtonInteractable(false);
        string userId = SaveDataManager.Inst.GetCutomId();

        Network.LoginAPI.Login(userId, (bool value) => {
            //Debug.Log($"CallbackLogin res:{value}");
            if (value) {
                Network.RankingAPI.GetUserRanking(
                    // 通信成功
                    (List<PlayerLeaderboardEntry> rankingList) => {
                        GameManager.Inst.LoadingView.Hide();
                        var info = rankingList[0];
                        Debug.Log($"{info.Position + 1,5}位：{info.StatValue,5}：{info.DisplayName}\n");
                        if (info.StatValue == 0) {
                            info.StatValue = GameManager.Inst.InitialRate;
                        }
                        GameManager.Inst.SetCurrentRateInfo(info.StatValue, info.Position+1);

                        SetTitleMode(TitleMode.GameMode);
                    },
                    // 通信失敗
                    () => { Debug.Log("ランキング取得失敗"); });
            } else {
                GameManager.Inst.LoadingView.Hide();
                Debug.LogError("CallbackLogin Error");
            }
        });
    }

    public void OnClickNext() {
        _titleMode = TitleMode.GameMode;
        SetPanelActive(_titleMode);
    }

    public void OnClickSingle() {
        GameManager.Inst.BattleMode = Entity.BattleMode.Solo;
        GameManager.Inst.MultiMode = Entity.MultiMode.Normal;
        GameManager.Inst.SetViewPosition(Entity.ViewType.Battle);
    }

    public void OnClickOnline() {
        GameManager.Inst.LoadingView.Show();
        Network.UserAPI.GetBattleData((bool result, BattleTimesModel battleData) => {
            GameManager.Inst.LoadingView.Hide();
            if (result) {
                GameManager.Inst.MyUserModel.BattleData = battleData;
                GameManager.Inst.SetViewPosition(Entity.ViewType.Matching);
            }
        });
    }

    public void OnClickSetting() {
        GameManager.Inst.SetViewPosition(Entity.ViewType.Setting);
    }
}
