using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DebugBattleView : SingletonMonoBhv<DebugBattleView>,
    IBeginDragHandler, IDragHandler, IEndDragHandler {
    [SerializeField] private Text _txtTurn = default;
    [SerializeField] private BattleView _battleView = default;
    [SerializeField] private Text _txtResult = default;
    [SerializeField] private Text _txtOldRate = default;
    [SerializeField] private Text _txtNewRate = default;

    public void SetTurn(int turn) {
        if (this.gameObject.activeSelf && _txtTurn) {
            _txtTurn.text = turn.ToString();
        }
    }

    public void SetResultText(bool isWin) {
        if (_txtResult) {
            _txtResult.text = isWin ? "WIN" : "LOSE";
        }
    }

    public void SetOldRate(int rate1, int rate2) {
        if (_txtOldRate) {
            _txtOldRate.text = string.Format($"{rate1}  {rate2}");
        }
    }

    public void SetNewRate(int rate1) {
        if (_txtNewRate) {
            _txtNewRate.text = string.Format($"{rate1}");
        }
    }

    public void OnClickReaultAnime(bool isWin) => _battleView.PlayResultAnimation(GameManager.Inst.MyPlayerIndex);

    public void OnClickRank() => GameManager.Inst.MultiMode = Entity.MultiMode.Rank;

    public void OnClickLose() {
        if (GameManager.Inst.BattleMode == Entity.BattleMode.Multi) {
            _battleView.GameOver(1 - GameManager.Inst.MyPlayerIndex);
        } else {
            _battleView.GameOver(1 - GameManager.Inst.Turn);
        }
    }

    public void OnBeginDrag(PointerEventData eventData) {
    }

    public void OnDrag(PointerEventData eventData) {
        this.transform.position = eventData.position;
    }

    public void OnEndDrag(PointerEventData eventData) {
    }
}
