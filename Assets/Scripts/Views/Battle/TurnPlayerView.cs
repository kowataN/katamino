using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TurnPlayerView : MonoBehaviour
{
    [SerializeField] private Animator _animator = default;
    [SerializeField] private Text _text = default;


    public void Init() {
        SetActiveState(0);
    }

    public void PlayAnimation(string msg) {
        //Debug.Log($"PlayAnimation msg:{msg}");
        _text.text = msg;
        SetActiveState(1);
        _animator.Play("ShowTurnPlayer", 0, 0.0f);
    }

    private void SetActiveState(int value) {
        this.gameObject.SetActive(value == 0 ? false : true);
    }
}
