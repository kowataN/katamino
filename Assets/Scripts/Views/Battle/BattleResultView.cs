using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattleResultView : MonoBehaviour {
    [SerializeField] private Animator _animator = default;
    [SerializeField] private Text _message = default;
    [SerializeField] private Button _btnBack = default;
    [SerializeField] private Button _btnRetry = default;
    [SerializeField] private Button _btnResult = default;

    public Action OnClickBackCallback;
    public Action OnClickRetryCallback;
    public Action EndAnimationCallback;

    public void SetCallback(Action backCallback, Action retryCallback) {
        OnClickBackCallback = backCallback;
        OnClickRetryCallback = retryCallback;
    }

    public void Init() {
        this.gameObject.SetActive(false);
        _btnRetry.gameObject.SetActive(GameManager.Inst.BattleMode == Entity.BattleMode.Solo);
        _btnBack.gameObject.SetActive(GameManager.Inst.MultiMode != Entity.MultiMode.Rank);
        _btnResult.gameObject.SetActive(GameManager.Inst.MultiMode == Entity.MultiMode.Rank);
    }

    public void PlayAnimation(bool isWin, string msg) {
        _message.text = msg;
        _message.color = isWin ? Color.red : Color.blue;
        this.gameObject.SetActive(true);
        _animator.Play("ShowResult", 0, 0.0f);
    }

    public void OnClickBack() => OnClickBackCallback?.Invoke();
    public void OnClickRetry() => OnClickRetryCallback?.Invoke();

    public void EndAnimation() => EndAnimationCallback?.Invoke();
}
