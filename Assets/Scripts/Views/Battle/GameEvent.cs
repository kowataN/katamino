﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEvent : MonoBehaviour
{
    public static Action<int> GameOver;

    public static Action CalcRate;

    public static Action CheckIfMinoCanBePlaced;

    public static Action MoveMinoToStartPosition;

    public static Action MinoPlacedComplete;
}
