﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattleView : MonoBehaviour {
    [SerializeField] private Canvas _canvas = default;
    public Canvas Canvas => _canvas;
    private MatchingManager _matchingManager = default;

    [SerializeField] private GridView _gridView = default;
    [Header("ターンプレイヤー関連")]
    [SerializeField] private Text[] _txtPlayerName = default;
    [SerializeField] private Text[] _txtTurnMarker = default;
    [SerializeField] private TurnPlayerView _turnPlayerView = default;
    [Header("結果パネル関連")]
    [SerializeField] private BattleResultView _battleResultView = default;

    [SerializeField] private StampPresenter _stampPresenter = default;
    
    public ReceiveStampView ReceiveStampView => _receiveStampView;
    [SerializeField] private ReceiveStampView _receiveStampView = default;

    private void Awake() {
        GameEvent.GameOver += GameOver;
        GameEvent.MinoPlacedComplete += MinoPlacedComplete;

        _gridView.CreateGrid();

        _battleResultView.SetCallback(() => {
            // 戻るボタン
            if (GameManager.Inst.BattleMode == Entity.BattleMode.Solo) {
                TransitView(Entity.ViewType.Title);
            } else {
                TransitView(Entity.ViewType.Matching);
            }
        },
        () => {
            // リトライ
            TransitView(Entity.ViewType.Battle);
        });

        _stampPresenter.SetObservable();
    }

    private void OnDestroy() {
        GameEvent.GameOver -= GameOver;
        GameEvent.MinoPlacedComplete -= MinoPlacedComplete;
        _battleResultView.SetCallback(null, null);
    }

    public void SetMatchingManager(MatchingManager matchingManager) {
        _matchingManager = matchingManager;
    }

    public void Init() {
        //Debug.Log($"BattleView::Init  Mode[{GameManager.Inst.BattleMode}]");
        PhotonManager.Inst.SendUpdateTurn(UnityEngine.Random.Range(0, 2));

        _txtTurnMarker[0].gameObject.SetActive(GameManager.Inst.Turn == 0);
        _txtTurnMarker[1].gameObject.SetActive(GameManager.Inst.Turn == 1);

        _matchingManager?.CallBackLeftPlayer.AddListener(CallBackLeftPlayer);

        _gridView.ResetGrid();
        _battleResultView.Init();

        if (GameManager.Inst.BattleMode == Entity.BattleMode.Multi) {
            _stampPresenter.Init();
        } else {
            _stampPresenter.HideView();
        }

        if (GameManager.Inst.BattleMode == Entity.BattleMode.Solo) {
            for (int i = 0; i < _txtPlayerName.Length; i++) {
                _txtPlayerName[i].text = "プレイヤー" + (i + 1).ToString();
            }
        } else {
            var playerList = PhotonManager.Inst.UserList;
            for (int i = 0; i < playerList.Length; i++) {
                _txtPlayerName[i].text = string.Format($"{playerList[i].NickName}");

                if (i == GameManager.Inst.MyPlayerIndex) {
                    _txtPlayerName[i].text += string.Format($"[{i}]");
                }
            }
        }

        DebugBattleView.Inst?.SetTurn(GameManager.Inst.Turn);

        if (GameManager.Inst.MultiMode == Entity.MultiMode.Rank) {
            int myOldRate = GameManager.Inst.MyUserModel.CrtRate;
            int oppnentOldRate = GameManager.Inst.OpponentUserModels.CrtRate;
            Debug.Log($"MyRate:{myOldRate}  OpRate:{oppnentOldRate}");
            DebugBattleView.Inst?.SetOldRate(myOldRate, oppnentOldRate);
            // 予め変動値を計算する
            GameManager.Inst.FloatingRate.WinValue = CalcRateIro.CalcRate(myOldRate, oppnentOldRate);
            GameManager.Inst.FloatingRate.LoseValue = CalcRateIro.CalcRate(oppnentOldRate, myOldRate);
            Debug.Log($"winVal:{GameManager.Inst.FloatingRate.WinValue} loseVal:{GameManager.Inst.FloatingRate.LoseValue}");
        }
    }

    private void TransitView(Entity.ViewType type) {
        if (GameManager.Inst.BattleMode == Entity.BattleMode.Multi) {
            _matchingManager.Disconnect();
        }
        GameManager.Inst.SetViewPosition(type);
    }

    private void CallBackLeftPlayer() {
        Debug.Log("対戦相手との接続が切れました。");
        GameManager.Inst.LoadingView.Hide();
        TransitView(Entity.ViewType.Matching);
    }

    public void PlayTurnPlayerAnimation() {
        if (GameManager.Inst.BattleMode == Entity.BattleMode.Solo) {
            _turnPlayerView.PlayAnimation(GameManager.Inst.Turn == 0 ? "プレイヤー1のターンです。" : "プレイヤー2のターンです。");
        } else {
            _turnPlayerView.PlayAnimation(GameManager.Inst.IsMyTurn ? "あなたのターンです。" : "相手のターンです。");
        }
    }

    public void UpdateTurnMarker() {
        _txtTurnMarker[0].gameObject.SetActive(GameManager.Inst.Turn == 0);
        _txtTurnMarker[1].gameObject.SetActive(GameManager.Inst.Turn == 1);

        DebugBattleView.Inst.SetTurn(GameManager.Inst.Turn);
    }

    public void GameOver(int winPlayerIdx) {
        GameManager.Inst.WinPlayerIdx = winPlayerIdx;
        GameManager.Inst.LockManager.Lock();
        if (GameManager.Inst.MultiMode == Entity.MultiMode.Normal) {
            // 通常バトル終了
            PhotonManager.Inst.SendBattleResult(winPlayerIdx);
        } else {
            // ランクバトル終了
            PhotonManager.Inst.SendEndRankBattle(winPlayerIdx);
        }
    }

    private void ResultEndAnimationCallback() {
        Debug.Log("ResultEndAnimationCallback");

        if (GameManager.Inst.MultiMode == Entity.MultiMode.Rank) {
            // バトルデータ更新
            if (GameManager.Inst.IsMyWin) {
                GameManager.Inst.MyUserModel.BattleData.WinTimes++;
            } else {
                GameManager.Inst.MyUserModel.BattleData.LoseTimes++;
            }

            GameManager.Inst.LoadingView.Show();
            Network.UserAPI.UpdateBattleData(GameManager.Inst.MyUserModel.BattleData, (bool result) => {
                GameManager.Inst.LoadingView.Hide();
                TransitView(Entity.ViewType.RankResult);
            });
        }
    }

    public void PlayResultAnimation(int winPlayerIdx) {
        //Debug.Log("PlayResultAnimation");
        GameManager.Inst.WinPlayerIdx = winPlayerIdx;
        _battleResultView.Init();

        if (GameManager.Inst.BattleMode == Entity.BattleMode.Multi) {
            _matchingManager?.CallBackLeftPlayer.RemoveListener(CallBackLeftPlayer);
        }

        bool isWin = GameManager.Inst.IsMyWin;
        DebugBattleView.Inst.SetResultText(isWin);
        if (GameManager.Inst.MultiMode == Entity.MultiMode.Normal) {
            _battleResultView.EndAnimationCallback = null;
        } else {
            Debug.Log("set ResultEndAnimationCallback");
            _battleResultView.EndAnimationCallback = ResultEndAnimationCallback;
        }

        if (GameManager.Inst.BattleMode == Entity.BattleMode.Solo) {
            _battleResultView.PlayAnimation(true, winPlayerIdx == 0 ? "プレイヤー1の勝利!!" : "プレイヤー2の勝利!!");
        } else {
            _battleResultView.PlayAnimation(isWin, isWin ? "あなたの勝利!!" : "あなたの敗北...");
        }
    }

    private void MinoPlacedComplete() {
        //Debug.Log("MinoPlacedComplete");
        PhotonManager.Inst.SendUpdateTurn(1 - GameManager.Inst.Turn);
    }

    public void SetGridViewOfMino(int minoIndex, int[] squareIndex) {
        Debug.Log($"SetGridViewOfMino Idx[{minoIndex.ToString()}]");
        AudioManager.Inst.PlayDropMino();
        _gridView.SetGridSquareOfMino(minoIndex, squareIndex);
        _gridView.SetMinoActive(minoIndex, false);
    }

    public void SetOwner() => _gridView.SetOwner();
}
