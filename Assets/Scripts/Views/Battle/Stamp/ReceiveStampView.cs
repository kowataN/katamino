using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ReceiveStampView : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _text = default;
    [SerializeField] private Animator _animator = default;

    public bool _isPlaying = false;

    public void PlayAnime(int stampIdx) {
        if (_isPlaying) {
            return;
        }

        _text.text = string.Format($"<sprite={stampIdx}>");

        _animator.Play("ReceiveStamp", 0, 0);
    }
}
