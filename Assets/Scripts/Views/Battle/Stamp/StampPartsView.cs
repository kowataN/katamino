using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class StampPartsView : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _text;
    [SerializeField] private Button _button;

    private int _imageTag = 7;

    public int ImageTag => _imageTag;
    public Button Button => _button;

    public void Init(int imageTag) {
        _imageTag = imageTag;
        _text.text = string.Format($"<sprite={_imageTag}>");
    }
}
