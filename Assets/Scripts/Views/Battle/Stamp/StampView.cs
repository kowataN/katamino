using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StampView : MonoBehaviour
{
    public Button Button => _button;
    [SerializeField] private Button _button = default;
    [SerializeField] private GameObject _partsPanel = default;

    public bool IsShow { get => _partsPanel.activeSelf; }
}
