using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MinoSquareView : MonoBehaviour
{
    [SerializeField] private Image _minoImage;
    public Image MinoImage => _minoImage;

    [SerializeField] private Image _occupiedImage;

    private void Start() {
        _occupiedImage.gameObject.SetActive(false);
    }

    public void SetMinoSprite(Sprite sprite) {
        _minoImage.sprite = sprite;
    }

    public void SetActivate(bool active) {
        gameObject.GetComponent<BoxCollider2D>().enabled = active;
        gameObject.SetActive(active);
    }

    public void SetOccupied(bool value) {
        _occupiedImage.gameObject.SetActive(value);
    }
}
