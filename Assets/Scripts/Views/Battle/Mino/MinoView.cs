﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using static MinoData;

public class MinoView : MonoBehaviour, IPointerClickHandler, IPointerUpHandler,
    IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerDownHandler {
    public int ImageIndex => _imageIndex;
    [SerializeField] private int _imageIndex = 0;

    public MinoSquareView MinoSquareView {
        get { return _squareMinoImage.GetComponent<MinoSquareView>(); }
    }
    [SerializeField] private GameObject _squareMinoImage = default;

    public Vector3 MinoSelectedScale = default;

    public PhotonView PhotonView { get { return _photonView; } }
    [SerializeField] private PhotonView _photonView = default;

    public PhotonTransformView PhotonTransformView {
        get { return _photonTransformView; }
    }
    [SerializeField] private PhotonTransformView _photonTransformView = default;

    [HideInInspector] private MinoData _currentMinoData = default;
    public MinoData CrtMinoData => _currentMinoData;

    public int TotalSquareNumber { get; set; }

    [SerializeField] private List<GameObject> _currentMino = new List<GameObject>();
    [SerializeField] private Vector3 _minoStartScale = default;
    private RectTransform _transform = default;
    private bool _minoDragging = false;

    public Vector3 StartPosision => _startPosition;
    [SerializeField] private Vector3 _startPosition = default;

    private bool _minoActive = true;

    public void SetInitStartPosition() {
        _startPosition = _transform.localPosition;
    }

    private void Awake() {
        _minoStartScale = this.GetComponent<RectTransform>().localScale;
        _transform = this.GetComponent<RectTransform>();
        _minoDragging = false;
        _minoActive = true;
        _imageIndex = 0;
    }

    private void OnEnable() {
        GameEvent.MoveMinoToStartPosition += MoveMinoToStartPositionAndScale;
    }

    private void OnDisable() {
        GameEvent.MoveMinoToStartPosition -= MoveMinoToStartPositionAndScale;
    }

    public void Init() {
        if (GameManager.Inst.BattleMode == Entity.BattleMode.Multi) {
            if (_photonView.AmOwner) {
                _photonView.RefreshRpcMonoBehaviourCache();
            }
            if (PhotonManager.Inst.IsMasterClient) {
                PhotonNetwork.CleanRpcBufferIfMine(_photonView);
            }
        } else {
            _photonView.StopAllCoroutines();
            _photonTransformView.StopAllCoroutines();
        }
        this.transform.localEulerAngles = new Vector3(0f, 0f, 0f);
        SetActivate(true);
        MoveMinoToStartPositionAndScale();
        PhotonTransformView.enabled = true;
    }

    public bool IsOnStartPosition() {
        return _transform.localPosition == _startPosition;
    }

    public bool IsAnyOfMinoSquareActive() {
        foreach (var square in _currentMino) {
            if (square.gameObject.activeSelf) {
                return true;
            }
        }
        return false;
    }

    public void SetActivate(bool active) {
        foreach (var square in _currentMino) {
            square?.GetComponent<MinoSquareView>().SetActivate(active);
        }

        _minoActive = active;
    }

    public void SetOwner() {
        var newPlayer = PhotonNetwork.PlayerList[GameManager.Inst.Turn];
        _photonView.TransferOwnership(newPlayer);
    }

    public void CreateMino(int imageIndex, MinoData minoData, Sprite minoSprite) {
        _imageIndex = imageIndex;
        _currentMinoData = minoData;
        _minoActive = true;
        SetInitStartPosition();
        TotalSquareNumber = GetNumberOfSquare(minoData);

        while (_currentMino.Count < TotalSquareNumber) {
            GameObject minoImage = Instantiate(_squareMinoImage, transform) as GameObject;
            minoImage.GetComponentInChildren<MinoSquareView>().SetMinoSprite(minoSprite);
            _currentMino.Add(minoImage);
        }

        foreach (var square in _currentMino) {
            square.gameObject.transform.position = Vector3.zero;
            square.gameObject.SetActive(false);
            square.GetComponentInChildren<MinoSquareView>().SetOccupied(false);
        }

        var SquareRect = _squareMinoImage.GetComponent<RectTransform>();
        var moveDistance = new Vector2(SquareRect.rect.width * SquareRect.localScale.x,
            SquareRect.rect.height * SquareRect.localScale.y);
        int currentIndexInList = 0;
        for (var row = 0; row < minoData.Rows; ++row) {
            for (var col = 0; col < minoData.Columns; ++col) {
                if (minoData.Board[row].Column[col]) {
                    _currentMino[currentIndexInList].SetActive(true);
                    float posX = GetXPositionForMinoSquare(minoData, col, moveDistance);
                    float posY = GetYPositionForMinoSquare(minoData, row, moveDistance);
                    _currentMino[currentIndexInList].GetComponent<RectTransform>().localPosition =
                        new Vector2(posX, posY);

                    currentIndexInList++;
                }
            }
        }
    }

    private void SetMinoSprite() {
        var minoSquareView = _squareMinoImage.GetComponentInChildren<MinoSquareView>();
    }

    private float GetXPositionForMinoSquare(MinoData minoData, int col, Vector2 moveDistance) {
        float shiftOnX = 0f;

        // 縦の位置計算
        if (minoData.Columns <= 1) {
            return shiftOnX;
        }

        if (minoData.Columns % 2 != 0) {
            var middleSquareIndex = (minoData.Columns - 1) / 2;
            var multiplier = (minoData.Columns - 1) / 2;

            shiftOnX = moveDistance.x * -(middleSquareIndex - col);
        } else {
            var middleSquareIndex = (minoData.Columns / 2) - col;
            var multiplier = minoData.Columns / 2;
            shiftOnX = moveDistance.x * middleSquareIndex * -1;
        }

        return shiftOnX;
    }

    private float GetYPositionForMinoSquare(MinoData minoData, int row, Vector2 moveDistance) {
        float shiftOnY = 0f;

        if (minoData.Rows <= 1) {
            return shiftOnY;
        }

        if (minoData.Rows % 2 != 0) {
            // 奇数
            var middleSquareIndex = (minoData.Rows - 1) / 2;
            var multiplier = (minoData.Rows - 1) / 2;

            shiftOnY = moveDistance.y * (middleSquareIndex - row);
        } else {
            // 偶数
            var middleSquareIndex1 = (minoData.Rows / 2) - row;
            var multiplier = minoData.Rows / 2;
            shiftOnY = moveDistance.y * middleSquareIndex1;
        }
        return shiftOnY;
    }

    private int GetNumberOfSquare(MinoData minoData) {
        int number = 0;
        foreach (var rowData in minoData.Board) {
            foreach (var active in rowData.Column) {
                if (active) {
                    number++;
                }
            }
        }
        return number;
    }

    public Row[] GetBoardToRot(float rot, out int outCol, out int outRow) {
        Row[] crtBoard;
        int rowMax = _currentMinoData.Board.Length;
        int colMax = _currentMinoData.Board[0].Column.Length;
        var rotZ = rot;
        var rotZdiff = rotZ / 90;
        outCol = colMax;
        outRow = rowMax;
        if (rotZdiff == 1 || rotZdiff == 3) {
            crtBoard = new Row[_currentMinoData.Board[0].Column.Length];
            outRow = _currentMinoData.Board[0].Column.Length;
            outCol = rowMax;
            for (int colIdx = 0; colIdx < colMax; ++colIdx) {
                crtBoard[colIdx] = new Row(rowMax);
                crtBoard[colIdx].Column = new bool[rowMax];
            }

            if (rotZdiff == 1) {
                for (var rowIdx = 0; rowIdx < rowMax; ++rowIdx) {
                    for (var colIdx = 0; colIdx < colMax; ++colIdx) {
                        var crtVal = _currentMinoData.Board[rowIdx].Column[colIdx];
                        var crtCol = colMax - 1 - colIdx;
                        crtBoard[crtCol].Column[rowIdx] = _currentMinoData.Board[rowIdx].Column[colIdx];
                    }
                }
            } else if (rotZdiff == 3) {
                for (var rowIdx = 0; rowIdx < rowMax; ++rowIdx) {
                    for (var colIdx = 0; colIdx < colMax; ++colIdx) {
                        var crtVal = _currentMinoData.Board[rowIdx].Column[colIdx];
                        var crtRow = rowMax - 1 - rowIdx;
                        crtBoard[colIdx].Column[crtRow] = _currentMinoData.Board[rowIdx].Column[colIdx];
                    }
                }
            }
        } else if (rotZdiff == 2) {
            crtBoard = new Row[_currentMinoData.Board.Length];
            for (int rowIdx = 0; rowIdx < rowMax; ++rowIdx) {
                crtBoard[rowIdx] = new Row(colMax);
                crtBoard[rowIdx].Column = new bool[colMax];
            }

            for (var rowIdx = 0; rowIdx < rowMax; ++rowIdx) {
                for (var colIdx = 0; colIdx < colMax; ++colIdx) {
                    var crtVal = _currentMinoData.Board[rowIdx].Column[colIdx];
                    var crtRow = rowMax - 1 - rowIdx;
                    var crtCol = colMax - 1 - colIdx;
                    crtBoard[crtRow].Column[crtCol] = crtVal;
                }
            }

        } else {
            crtBoard = _currentMinoData.Board;
        }

        return crtBoard;
    }

    public void OnPointerClick(PointerEventData eventData) {
        if (!GameManager.Inst.IsMyTurn) {
            return;
        }

        if (_minoDragging) {
            return;
        }
        this.transform.localEulerAngles += new Vector3(0.0f, 0.0f, -90.0f);
    }

    public void OnPointerUp(PointerEventData eventData) {
    }

    public void OnBeginDrag(PointerEventData eventData) {
        if (!GameManager.Inst.IsMyTurn || GameManager.Inst.IsLock) {
            return;
        }

        Debug.Log($"idx[{_imageIndex}]");
        _minoDragging = true;
        this.GetComponent<RectTransform>().localScale = MinoSelectedScale;
    }

    public void OnDrag(PointerEventData eventData) {
        if (!GameManager.Inst.IsMyTurn || GameManager.Inst.IsLock) {
            return;
        }

        _transform.anchorMin = Vector2.zero;
        _transform.anchorMax = Vector2.zero;

        _transform.position = eventData.position;
    }

    public void OnEndDrag(PointerEventData eventData) {
        if (!GameManager.Inst.IsMyTurn || GameManager.Inst.IsLock) {
            return;
        }

        _minoDragging = false;
        this.GetComponent<RectTransform>().localScale = _minoStartScale;
        GameEvent.CheckIfMinoCanBePlaced?.Invoke();
    }

    public void OnPointerDown(PointerEventData eventData) {
    }

    public void MoveMinoToStartPositionAndScale() {
        this.GetComponent<RectTransform>().localScale = _minoStartScale;
        _transform.localPosition = _startPosition;
    }
}
