using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinoStorage : MonoBehaviour
{
    [SerializeField] private List<MinoData> _minoDataList;
    [SerializeField] private List<MinoView> _minoViewList;
    public List<MinoView> MinoViewList => _minoViewList;
    [SerializeField] private List<Sprite> _minoSprite;

    void Start()
    {
        //CreateMinos();
    }

    /// <summary>
    /// ミノを作成します。
    /// </summary>
    public void CreateMinos() {
        int index = 0;
        foreach (var mino in _minoViewList) {
            mino.CreateMino(index, _minoDataList[index], _minoSprite[index]);
            index++;
        }
    }

    public MinoView GetCurrentSelectedMino() {
        foreach (var mino in _minoViewList) {
            if (mino.IsOnStartPosition() == false && mino.IsAnyOfMinoSquareActive()) {
                return mino;
            }
        }
        return null;
    }

    public Sprite GetMinoSprite(int index) {
        return _minoSprite[index];
    }

    public void InitMino() {
        foreach (var mino in _minoViewList) {
            mino.Init();
        }
    }

    public void SetOwner() {
        foreach (var mino in _minoViewList) {
            mino.SetOwner();
        }
    }
}
