using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


[CustomEditor(typeof(MinoData), false)]
[CanEditMultipleObjects]
[System.Serializable]
public class MinoDataDrawer : Editor
{
    private MinoData _minoDataInstance => target as MinoData;

    public override void OnInspectorGUI() {
        serializedObject.Update();
        ClearBoardButton();
        EditorGUILayout.Space();

        DrawColumnsInputFields();
        EditorGUILayout.Space();

        if (_minoDataInstance.Board != null && _minoDataInstance.Columns > 0 && _minoDataInstance.Rows > 0) {
            DrawBoardTable();
        }

        serializedObject.ApplyModifiedProperties();

        if (GUI.changed) {
            EditorUtility.SetDirty(_minoDataInstance);
        }
    }

    private void ClearBoardButton() {
        if (GUILayout.Button("ボード初期化")) {
            _minoDataInstance.Clear();
        }
    }

    private void DrawColumnsInputFields() {
        var colTemp = _minoDataInstance.Columns;
        var rowTemp = _minoDataInstance.Rows;

        _minoDataInstance.Columns = EditorGUILayout.IntField("横(col)", _minoDataInstance.Columns);
        _minoDataInstance.Rows = EditorGUILayout.IntField("縦(row)", _minoDataInstance.Rows);

        if ((_minoDataInstance.Columns != colTemp || _minoDataInstance.Rows != rowTemp) &&
           _minoDataInstance.Columns > 0 && _minoDataInstance.Rows > 0) {
            _minoDataInstance.CreateNewBoard();
        }
    }

    private void DrawBoardTable() {
        var tableStyle = new GUIStyle("box");
        tableStyle.padding = new RectOffset(10, 10, 10, 10);
        tableStyle.margin.left = 32;

        var headerColumnStyle = new GUIStyle();
        headerColumnStyle.fixedWidth = 65;
        headerColumnStyle.alignment = TextAnchor.MiddleCenter;

        var rowStyle = new GUIStyle();
        rowStyle.fixedHeight = 25;
        rowStyle.alignment = TextAnchor.MiddleCenter;

        var dataFieldStyle = new GUIStyle(EditorStyles.miniButtonMid);
        dataFieldStyle.normal.background = Texture2D.grayTexture;
        dataFieldStyle.onNormal.background = Texture2D.whiteTexture;

        for (var row=0; row< _minoDataInstance.Rows; ++row) {
            EditorGUILayout.BeginHorizontal(headerColumnStyle);

            for (var col=0; col< _minoDataInstance.Columns; ++col) {
                EditorGUILayout.BeginHorizontal(rowStyle);
                var data = EditorGUILayout.Toggle(_minoDataInstance.Board[row].Column[col], dataFieldStyle);
                _minoDataInstance.Board[row].Column[col] = data;
                EditorGUILayout.EndHorizontal();
            }

            EditorGUILayout.EndHorizontal();
        }
    }
}
