using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static MinoData;

public class GridView : MonoBehaviour {
    [SerializeField] private MinoStorage _minoStorage = default;
    [SerializeField] private int _columns = 8;
    [SerializeField] private int _rows = 8;
    [SerializeField] private float _squaresGap = 3f;
    [SerializeField] private GameObject _prefabGridSquare = default;
    [SerializeField] private Vector2 _startPosition = new Vector2(-400.0f, 400.0f);
    [SerializeField] private float _squareScale = 1.0f;
    [SerializeField] private float _everySquareOffSet = 0.5f;
    [SerializeField] private LineIndicator _lineIndicator;

    private Vector2 _offset = new Vector2(0.0f, 0.0f);
    private List<GameObject> _gridSquares = new List<GameObject>();

    private void OnEnable() {
        GameEvent.CheckIfMinoCanBePlaced += CheckIfMinoCanBePlaced;
    }

    private void OnDisable() {
        GameEvent.CheckIfMinoCanBePlaced -= CheckIfMinoCanBePlaced;
    }

    public void ResetGrid() {
        InitGrid();
        InitMinos();
    }

    private void InitMinos() {
        _minoStorage.CreateMinos();
        _minoStorage.InitMino();
    }

    public void CreateGrid() {
        SpawnGridSquares();
        SetGridSquaresPosition();
    }

    private void InitGrid() {
        foreach (var gridObj in _gridSquares) {
            gridObj.transform.localScale = new Vector3(_squareScale, _squareScale, _squareScale);
            GridSquare girdSquare = gridObj.GetComponent<GridSquare>();
            girdSquare.Init();
        }
    }

    private void SpawnGridSquares() {
        int squareIndex = 0;
        for (var row = 0; row < _rows; ++row) {
            for (var column = 0; column < _columns; ++column) {
                _gridSquares.Add(Instantiate(_prefabGridSquare) as GameObject);

                _gridSquares[_gridSquares.Count - 1].GetComponent<GridSquare>().SquareIndex = squareIndex;
                _gridSquares[_gridSquares.Count - 1].transform.SetParent(this.transform);
                _gridSquares[_gridSquares.Count - 1].transform.localScale = new Vector3(_squareScale, _squareScale, _squareScale);
                _gridSquares[_gridSquares.Count - 1].GetComponent<GridSquare>().SetImage();
                squareIndex++;
            }
        }
    }

    private void SetGridSquaresPosition() {
        int colNumber = 0;
        int rowNumber = 0;
        Vector2 squareGapNumber = new Vector2(0.0f, 0.0f);
        bool rowMoved = false;

        var squareRect = _gridSquares[0].GetComponent<RectTransform>();

        _offset.x = squareRect.rect.width * squareRect.transform.localScale.x + _everySquareOffSet;
        _offset.y = squareRect.rect.height * squareRect.transform.localScale.y + _everySquareOffSet;

        foreach (GameObject square in _gridSquares) {
            if (colNumber + 1 > _columns) {
                squareGapNumber.x = 0;
                colNumber = 0;
                rowNumber++;
                rowMoved = false;
            }

            var posXOffset = _offset.x * colNumber + (squareGapNumber.x * _squaresGap);
            var posYOffset = _offset.y * rowNumber + (squareGapNumber.y * _squaresGap);

            if (colNumber > 0 && colNumber % 3 == 0) {
                squareGapNumber.x++;
                posXOffset += _squaresGap;
            }

            if (rowNumber > 0 && rowNumber % 3 == 0 && rowMoved == false) {
                rowMoved = true;
                squareGapNumber.y++;
                posYOffset += _squaresGap;
            }

            square.GetComponent<RectTransform>().anchoredPosition =
                new Vector2(_startPosition.x + posXOffset,
                            _startPosition.y - posYOffset);

            square.GetComponent<RectTransform>().localPosition =
                new Vector3(_startPosition.x + posXOffset,
                            _startPosition.y - posYOffset,
                            0.0f);

            colNumber++;
        }
    }

    private void CheckIfMinoCanBePlaced() {
        var squareIndex = new List<int>();
        bool isPlaced = false;

        foreach (var square in _gridSquares) {
            var gridSquare = square.GetComponent<GridSquare>();
            if (gridSquare.Selected && !gridSquare.SquareOccupied) {
                squareIndex.Add(gridSquare.SquareIndex);
                gridSquare.Selected = false;
            }
        }

        var crtSelectedMino = _minoStorage.GetCurrentSelectedMino();
        if (crtSelectedMino == null) {
            // 選択されていなかったら終了
            return;
        }

        if (crtSelectedMino.TotalSquareNumber == squareIndex.Count) {
            // ミノ設置
            isPlaced = true;
            PhotonManager.Inst.SendDropMino(crtSelectedMino.ImageIndex, squareIndex.ToArray());
        } else {
            AudioManager.Inst.PlayCancel();
            GameEvent.MoveMinoToStartPosition?.Invoke();
        }

        var res = CheckIfPlayerLost();
        if (res) {
            // 置ける場所がないので負け
            //Debug.Log("GameOver");
            GameEvent.GameOver?.Invoke(GameManager.Inst.Turn);
        }
        else {
            if (isPlaced) {
                GameEvent.MinoPlacedComplete?.Invoke();
            }
        }
    }

    private bool CheckIfPlayerLost() {
        var validMinos = 0;
        for (var idx=0; idx<_minoStorage.MinoViewList.Count; ++idx) {
            var isMinoActive = _minoStorage.MinoViewList[idx].IsAnyOfMinoSquareActive();
            for (var i = 0; i < 4; i++) {
                int outCol, outRow;
                var minoView = _minoStorage.MinoViewList[idx];
                var board = minoView.GetBoardToRot(i * 90, out outCol, out outRow);
                if (CheckIfMinoCanBePlaceOnGrid(board, outCol, outRow, minoView.TotalSquareNumber) && isMinoActive) {
                    _minoStorage.MinoViewList[idx]?.SetActivate(true);
                    validMinos++;
                }
            }
        }

        return validMinos == 0;
    }

    /// <summary>
    /// ミノが盤面に配置出来るか判定
    /// </summary>
    /// <param name="crtMino"></param>
    private bool CheckIfMinoCanBePlaceOnGrid(Row[] crtBoard, int minoColumns, int minoRows, int totalSquareNumber) {
        List<int> originMinoFillUpMino = new List<int>();
        var minoIndex = 0;

        for (var rowIdx = 0; rowIdx < minoRows; ++rowIdx) {
            for (var colIdx = 0; colIdx < minoColumns; ++colIdx) {
                if (crtBoard[rowIdx].Column[colIdx]) {
                    originMinoFillUpMino.Add(minoIndex);
                }

                minoIndex++;
            }
        }

        if (totalSquareNumber != originMinoFillUpMino.Count) {
            Debug.LogError("エラー");
        }

        var minoList = GetAllSquaresCombination(minoColumns, minoRows);

        bool canBePlaced = false;
        foreach (var number in minoList) {
            bool minoCanBePlaced = true;
            foreach (var minoIndexCheck in originMinoFillUpMino) {
                var comp = _gridSquares[number[minoIndexCheck]].GetComponent<GridSquare>();
                if (comp.SquareOccupied) {
                    minoCanBePlaced = false;
                }
            }

            if (minoCanBePlaced) {
                canBePlaced = true;
            }
        }

        return canBePlaced;
    }

    private List<int[]> GetAllSquaresCombination(int cols, int rows) {
        var minoList = new List<int[]>();
        var lastColIndex = 0;
        var lastRowIndex = 0;

        int safeIndex = 0;

        while (lastRowIndex + (rows-1) < _rows) {
            var rowData = new List<int>();

            for (int row = lastRowIndex; row < lastRowIndex + rows; row++) {
                for (int col = lastColIndex; col < lastColIndex + cols; col++) {
                    rowData.Add(_lineIndicator.line_data[row, col]);
                }
            }

            minoList.Add(rowData.ToArray());

            lastColIndex++;

            if (lastColIndex + (cols-1) >= 8) {
                lastRowIndex++;
                lastColIndex = 0;
            }

            safeIndex++;
            if (safeIndex > 100) {
                break;
            }
        }

        return minoList;
    }

    public void SetGridSquareOfMino(int minoIndex, int[] squareIndex) {
        var sprite = _minoStorage.GetMinoSprite(minoIndex);
        foreach (var index in squareIndex) {
            _gridSquares[index].GetComponent<GridSquare>().SetActiveImage(sprite);
            _gridSquares[index].GetComponent<GridSquare>().PlaceMinoOnBoard();
        }
    }

    public void SetMinoActive(int minoIndex, bool value) {
        _minoStorage.MinoViewList[minoIndex].PhotonTransformView.enabled = false;
        _minoStorage.MinoViewList[minoIndex].SetActivate(value);
        _minoStorage.MinoViewList[minoIndex].MoveMinoToStartPositionAndScale();
    }

    public void SetOwner() => _minoStorage.SetOwner();
}
