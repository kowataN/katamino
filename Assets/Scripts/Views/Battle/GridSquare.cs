using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GridSquare : MonoBehaviour {
    [SerializeField] private Image _normalImage = default;
    [SerializeField] private Image _hoverImage = default;
    [SerializeField] private Image _activeImage = default;
    [SerializeField] private Sprite _spriteImage = default;

    public bool Selected { get; set; }
    public int SquareIndex { get; set; }
    public bool SquareOccupied { get; set; }

    private void Start() {
        Init();
    }

    public void Init() {
        _hoverImage.gameObject.SetActive(false);
        _activeImage.gameObject.SetActive(false);
        Selected = false;
        SquareOccupied = false;
    }

    // 一時関数
    public bool CanWeUseThisSquare() {
        return _hoverImage.gameObject.activeSelf;
    }

    public void PlaceMinoOnBoard() {
        ActivateSquare();
    }

    public void ActivateSquare() {
        _hoverImage.gameObject.SetActive(false);
        _activeImage.gameObject.SetActive(true);
        Selected = true;
        SquareOccupied = true;
    }

    public void SetImage() => _normalImage.sprite = _spriteImage;
    public void SetActiveImage(Sprite sprite) => _activeImage.sprite = sprite;

    private void OnTriggerEnter2D(Collider2D collision) {
        if (SquareOccupied == false) {
            Selected = true;
            _hoverImage.gameObject.SetActive(true);
        } else if (collision.GetComponent<MinoSquareView>() !!= null) {
            collision.GetComponent<MinoSquareView>().SetOccupied(true);
        }
    }

    private void OnTriggerStay2D(Collider2D collision) {
        Selected = true;

        if (SquareOccupied == false) {
            _hoverImage.gameObject.SetActive(true);
        } else if (collision.GetComponent<MinoSquareView>()! != null) {
            collision.GetComponent<MinoSquareView>().SetOccupied(true);
        }
    }

    private void OnTriggerExit2D(Collider2D collision) {
        if (SquareOccupied == false) {
            Selected = false;
            _hoverImage.gameObject.SetActive(false);
        } else if (collision.GetComponent<MinoSquareView>()! != null) {
            collision.GetComponent<MinoSquareView>().SetOccupied(false);
        }
    }
}
