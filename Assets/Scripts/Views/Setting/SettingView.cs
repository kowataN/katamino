using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniRx;

public class SettingView : MonoBehaviour {
    [SerializeField] private InputField _inputName = default;
    [SerializeField] private Button _btnChanged = default;
    [SerializeField] private Text _txtCrtName = default;
    [SerializeField] private SoundSlider _sliderBGM = default;
    [SerializeField] private SoundSlider _sliderSE = default;

    private void Start() {
        _sliderSE.OnChangeValue.Subscribe(value => {
            AudioManager.Inst.PlayTap();
        });
    }

    public void Init() {
        Debug.Log("SettingView::Init");
        _inputName.text = "";
        _btnChanged.interactable = false;
        SetCrtUserName();
    }

    private void SetCrtUserName() => _txtCrtName.text = "現在の名前：" + GameManager.Inst.MyUserModel.UserName;

    public void OnChangedValue() => _btnChanged.interactable = _inputName.text.Length > 0;

    public void OnClickNameChanged() {
        GameManager.Inst.LoadingView.Show();
        string userId = SaveDataManager.Inst.GetCutomId();
        Network.UserAPI.UpdateUserName(userId, _inputName.text, (bool result) => {
            GameManager.Inst.LoadingView.Hide();
            SaveDataManager.Inst.SetUserName(_inputName.text);
            SaveDataManager.Inst.Save();
            GameManager.Inst.MyUserModel.UserName = SaveDataManager.Inst.GetUserName();
            _inputName.text = "";
            SetCrtUserName();
        });
    }

    public void OnClickBack() {
        // サウンド更新
        SaveDataManager.Inst.SaveAudio();

        GameManager.Inst.SetViewPosition(Entity.ViewType.Title);
    }

    public void OnChangeValueBGM() {
        AudioManager.Inst.SetVolumeBGM(_sliderBGM.GetValue() / 100.0f / 5.0f);
    }

    public void OnChangeValueSE() {
        AudioManager.Inst.SetVolumeSE(_sliderSE.GetValue() / 100.0f);
    }
}
