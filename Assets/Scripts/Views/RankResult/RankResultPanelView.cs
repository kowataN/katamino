using System;
using UnityEngine;
using UnityEngine.UI;

public class RankResultPanelView : MonoBehaviour {
    [SerializeField] private Animator _animator = default;
    [SerializeField] private Text _txtOldRank = default;
    [SerializeField] private Text _txtNewRank = default;
    [SerializeField] private Text _txtOldRanking = default;
    [SerializeField] private Text _txtNewRanking = default;

    public Action CallbackEndAnimation;

    public void Init() {
        this.gameObject.SetActive(false);
    }

    public void PlayAnimation(int oldRate, int newRate, int oldRanking, int newRanking) {
        _txtOldRank.text = RankString.GetString(oldRate);
        _txtNewRank.text = RankString.GetString(newRate);
        _txtOldRanking.text = oldRanking.ToString();
        _txtNewRanking.text = newRanking.ToString();
        this.gameObject.SetActive(true);
        _animator.Play("ShowRank", 0, 0.0f);
    }

    public void EndAnimation() {
        Debug.Log("EndAnimation");
        CallbackEndAnimation?.Invoke();
    }
}
