﻿using PlayFab.ClientModels;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RankResultView : MonoBehaviour {
    [SerializeField] private RankResultPanelView _rankResultPanelView = default;
    [SerializeField] private Button _btnBack = default;

    private int _oldRate;
    private int _oldRanking;

    private void Awake() {
        Debug.Log("RankResultView.Awake");
        _rankResultPanelView.CallbackEndAnimation += CallbackEndAnimation;
    }

    private void OnDestroy() {
        _rankResultPanelView.CallbackEndAnimation -= CallbackEndAnimation;
    }

    public void Init() {
        Debug.Log($"RankResultVIew::Init  Mode[{GameManager.Inst.BattleMode}]");
        Debug.Log($"MultiMode:{GameManager.Inst.MultiMode.ToString()}");

        GameManager.Inst.LockManager.Lock();
        GameManager.Inst.LoadingView.Show();

        _btnBack.interactable = false;

        _oldRate = GameManager.Inst.MyUserModel.CrtRate;
        _oldRanking = GameManager.Inst.MyUserModel.CrtRanking;

        int newRate = CalcRate();

        // レート更新
        Network.UserAPI.UpdateRate(newRate, (bool result, int rateValue) => {
            if (result) {
                // 最新のランキングを取得
                Network.RankingAPI.GetUserRanking(
                    // 通信成功
                    (List<PlayerLeaderboardEntry> rankingList) => {
                        GameManager.Inst.LoadingView.Hide();

                        var info = rankingList[0];
                        Debug.Log($"{info.Position + 1,5}位：{info.StatValue,5}：{info.DisplayName}\n");
                        GameManager.Inst.SetCurrentRateInfo(info.StatValue, info.Position+1);

                        var model = GameManager.Inst.MyUserModel;
                        _rankResultPanelView.PlayAnimation(_oldRate, model.CrtRate, _oldRanking, model.CrtRanking);
                    },
                    // 通信失敗
                    () => {
                        GameManager.Inst.LoadingView.Hide();
                        GameManager.Inst.SetViewPosition(Entity.ViewType.Matching);
                    });
            }
        });
    }

    private void CallbackEndAnimation() {
        GameManager.Inst.LockManager.UnLock();
        _btnBack.interactable = true;
    }

    /// <summary>
    /// 対戦後のレート計算します。
    /// </summary>
    /// <returns>更新後のレート</returns>
    private int CalcRate() {
        GameManager.Inst.LoadingView.Show();
        Debug.Log($"CalcRate MyIdx:{GameManager.Inst.MyPlayerIndex}  WinIdx:{GameManager.Inst.WinPlayerIdx}");

        // レート更新
        int myOldRate = GameManager.Inst.MyUserModel.CrtRate;
        int opOldRate = GameManager.Inst.OpponentUserModels.CrtRate;

        int winVal = GameManager.Inst.FloatingRate.WinValue;
        int loseVale = GameManager.Inst.FloatingRate.LoseValue;

        int newRate = 0;
        if (GameManager.Inst.IsMyWin) {
            newRate = Mathf.Clamp(myOldRate + winVal, 1000, myOldRate + winVal);
        } else {
            newRate = Mathf.Clamp(myOldRate - loseVale, 1000, myOldRate - loseVale);
        }
        DebugBattleView.Inst?.SetNewRate(newRate);

        Debug.Log($"myNewRate:{myOldRate} → {newRate}");

        return newRate;
    }

    public void OnClickBack() {
        GameManager.Inst.SetViewPosition(Entity.ViewType.Matching);
    }
}
