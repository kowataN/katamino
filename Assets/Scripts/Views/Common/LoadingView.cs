using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class LoadingView : MonoBehaviour {
    [SerializeField] private GameObject _mino = default;
    [SerializeField] private Button _button = default;

    public void Show() {
        _button.gameObject.SetActive(true);
        _mino.transform.localEulerAngles = new Vector3(0f, 0f, 0f);
        gameObject.SetActive(true);
        StartCoroutine(Proc());
    }

    public void Hide() {
        _button.gameObject.SetActive(false);
        gameObject.SetActive(false);
        StopCoroutine(Proc());
    }

    IEnumerator Proc() {
        while (true) {
            yield return RotateMino();
        }
    }

    IEnumerator RotateMino() {
        _mino.transform.localEulerAngles += new Vector3(0f, 0f, 10f);
        if ((int)_mino.transform.localEulerAngles.z % 90 == 0) {
            _mino.transform.localEulerAngles = new Vector3(0f, 0f, 0f);
            yield return new WaitForSeconds(0.5f);
        } else {
            yield return new WaitForSeconds(0.05f);
        }
    }
}
