using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 対戦回数モデル
/// </summary>
public class BattleTimesModel {
    /// <summary>
    /// 対戦回数
    /// </summary>
    public int BattleTimes {
        get { return WinTimes + LoseTimes; }
    }

    private int _winTimes;
    /// <summary>
    /// 勝利回数
    /// </summary>
    public int WinTimes {
        get { return _winTimes; }
        set { _winTimes = Mathf.Max(value, 0); }
    }

    private int _loseTimes;
    /// <summary>
    /// 敗北回数
    /// </summary>
    public int LoseTimes {
        get { return _loseTimes; }
        set { _loseTimes = Mathf.Max(value, 0); }
    }

    public BattleTimesModel() {
        Init();
    }

    public void Init() {
        _winTimes = 0;
        _loseTimes = 0;
    }
}
