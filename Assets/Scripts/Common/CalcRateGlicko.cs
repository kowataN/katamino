using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CalcRateGlicko
{
    public float Deviation = 50;

    public int CalcRate(int rate1, int rate2) {
        float oldRD = 350f;
        float newRD = 0f;

        // 事前RD算出
        if (rate1 == 1500) {
            // 初期値の場合は別計算
            oldRD = Mathf.Sqrt(50 * 50 + oldRD * oldRD);
        }
        Debug.Log($"事前RD:{oldRD}  newRD:{newRD}");
        return 0;
    }
}
