using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class RankString {
    public static string GetString(int rate) {
        if (rate < 1300) {
            return "D";
        } else if (rate < 1600) {
            return "C";
        } else if (rate < 2000) {
            return "B";
        } else if (rate < 2400) {
            return "A";
        } else if (rate >= 2400) {
            return "S";
        }
        return "";
    }
}
