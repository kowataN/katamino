using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CalcRateIro 
{
    public static int FloatingValue { get; set; } = 16;

    public static int CalcRate(int rate1, int rate2) {
        float rateDiff = (float)(rate2 - rate1);
        if (Mathf.Abs(rateDiff) >= 400) {
            return 1;
        }
        return (int)Mathf.Floor((float)FloatingValue + (rateDiff * 0.04f));
    }
}
