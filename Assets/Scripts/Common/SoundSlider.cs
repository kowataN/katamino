using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UniRx;
using UnityEngine.UI;
using System;

public class SoundSlider : MonoBehaviour, IPointerUpHandler, IPointerDownHandler
{
    [SerializeField] private Slider _slider = default;

    public float GetValue() => _slider.value;

    protected Subject<float> _subjectChangeValue = new Subject<float>();

    public IObservable<float> OnChangeValue {
        get { return _subjectChangeValue; }
    }

    public void OnPointerDown(PointerEventData eventData) {
        _subjectChangeValue.OnNext(_slider.value);
    }

    public void OnPointerUp(PointerEventData eventData) {
        _subjectChangeValue.OnNext(_slider.value);
    }
}
