using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class RankColors
{
    public static readonly Dictionary<string, Color> ColorList = new Dictionary<string, Color>() {
        { "S", new Color(1.0f, 0.31f, 0.0f) },
        { "A", new Color(1.0f, 1.0f, 0f) },
        { "B", new Color(0.5f, 1.0f, 0f) },
        { "C", new Color(0f, 1.0f, 0.5f) },
        { "D", new Color(0f, 0.5f, 1.0f) },
    };
}
